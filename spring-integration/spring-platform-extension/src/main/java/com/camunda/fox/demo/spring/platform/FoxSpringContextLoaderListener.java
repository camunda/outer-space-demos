package com.camunda.fox.demo.spring.platform;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.ContextLoaderListener;

import com.camunda.fox.client.impl.ProcessArchiveSupport;
import com.camunda.fox.platform.api.ProcessEngineService;

public class FoxSpringContextLoaderListener extends ContextLoaderListener {

  @Override
  protected void customizeContext(ServletContext servletContext, ConfigurableWebApplicationContext applicationContext) {
    super.customizeContext(servletContext, applicationContext);

    try {
      new InitialContext().bind("java:app/springContext", applicationContext);
      ProcessEngineService engineService = InitialContext.doLookup(ProcessArchiveSupport.PROCESS_ENGINE_SERVICE_NAME);

      DefaultListableBeanFactory newParentBeanFactory = new DefaultListableBeanFactory();
      newParentBeanFactory.registerSingleton("processEngine", engineService.getDefaultProcessEngine());

      GenericApplicationContext newParentContext = new GenericApplicationContext(newParentBeanFactory, applicationContext.getParent());
      newParentContext.refresh();

      applicationContext.setParent(newParentContext);

    } catch (NamingException e) {
      throw new RuntimeException("Could not lookup fox process engine service", e);
    }
  }

}
