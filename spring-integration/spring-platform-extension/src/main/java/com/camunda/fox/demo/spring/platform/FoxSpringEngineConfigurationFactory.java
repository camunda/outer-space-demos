package com.camunda.fox.demo.spring.platform;

import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import com.camunda.fox.platform.impl.configuration.JtaCmpeProcessEngineConfiguration;
import com.camunda.fox.platform.impl.configuration.spi.ProcessEngineConfigurationFactory;
import com.camunda.fox.platform.impl.service.ProcessEngineController;

/**
 * muesste ins ear der platform, bzw. beim jboss 7 als module dep. / resource
 * @author drobisch
 *
 */
public class FoxSpringEngineConfigurationFactory implements ProcessEngineConfigurationFactory {
  private ProcessEngineController processEngineController;

  public ProcessEngineConfigurationImpl getProcessEngineConfiguration() {
    ProcessEngineConfigurationImpl configuration = new JtaCmpeProcessEngineConfiguration(processEngineController) {
      @Override
      protected void initExpressionManager() {
        expressionManager = new FoxSpringExpressionManager(getProcessArchiveServices());
      }
    };
    return configuration;
  }

  public void setProcessEngineController(ProcessEngineController processEngineController) {
    this.processEngineController = processEngineController;
  }
}
