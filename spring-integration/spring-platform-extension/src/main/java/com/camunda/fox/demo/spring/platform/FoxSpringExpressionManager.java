package com.camunda.fox.demo.spring.platform;

import java.util.Map;

import org.activiti.engine.delegate.VariableScope;
import org.activiti.engine.impl.el.ExpressionManager;
import org.activiti.engine.impl.el.VariableScopeElResolver;
import org.activiti.engine.impl.javax.el.ArrayELResolver;
import org.activiti.engine.impl.javax.el.BeanELResolver;
import org.activiti.engine.impl.javax.el.CompositeELResolver;
import org.activiti.engine.impl.javax.el.ELResolver;
import org.activiti.engine.impl.javax.el.ListELResolver;
import org.activiti.engine.impl.javax.el.MapELResolver;
import org.springframework.context.ApplicationContext;

import com.camunda.fox.platform.impl.configuration.CmpeCdiResolver;
import com.camunda.fox.platform.impl.context.spi.ProcessArchiveServices;


public class FoxSpringExpressionManager extends ExpressionManager {
  protected ApplicationContext applicationContext;
  protected Map<Object, Object> beans;
  private ProcessArchiveServices paServices;
  
  public FoxSpringExpressionManager(ProcessArchiveServices paServices) {
	this.paServices = paServices;
  }

  @Override
  protected ELResolver createElResolver(VariableScope variableScope) {
    CompositeELResolver compositeElResolver = new CompositeELResolver();
    compositeElResolver.add(new VariableScopeElResolver(variableScope));
    
    compositeElResolver.add(new FoxSpringContextElResolver());
    compositeElResolver.add(new CmpeCdiResolver(paServices));
    
    compositeElResolver.add(new ArrayELResolver());
    compositeElResolver.add(new ListELResolver());
    compositeElResolver.add(new MapELResolver());
    compositeElResolver.add(new BeanELResolver());
    return compositeElResolver;
  }
}
