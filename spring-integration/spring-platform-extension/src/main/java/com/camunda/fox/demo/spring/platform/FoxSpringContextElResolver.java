package com.camunda.fox.demo.spring.platform;

import java.beans.FeatureDescriptor;
import java.lang.reflect.Method;
import java.util.Iterator;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.impl.javax.el.ELContext;
import org.activiti.engine.impl.javax.el.ELResolver;


public class FoxSpringContextElResolver extends ELResolver {
  
  private static final String SPRING_CONTEXT_NAME = "java:app/springContext";

  public FoxSpringContextElResolver() {
  }

  public Object getValue(ELContext context, Object base, Object property) {
    try {
      ContextWrapper applicationContext = new ContextWrapper(InitialContext.doLookup(SPRING_CONTEXT_NAME));
      if (base == null) {
        // according to javadoc, can only be a String
        String key = (String) property;

        if (applicationContext.containsBean(key)) {
          context.setPropertyResolved(true);
          return applicationContext.getBean(key);
        }
      }

      return null;
      
    } catch (NamingException e) {
      throw new RuntimeException("Could not find application context for spring EL resolver", e);
    }
  }

  public boolean isReadOnly(ELContext context, Object base, Object property) {
    return true;
  }

  public void setValue(ELContext context, Object base, Object property, Object value) {
    
    try {
      ContextWrapper applicationContext = new ContextWrapper(InitialContext.doLookup(SPRING_CONTEXT_NAME));
      if(base == null) {
        String key = (String) property;
        if (applicationContext.containsBean(key)) {
          throw new ActivitiException("Cannot set value of '" + property + 
            "', it resolves to a bean defined in the Spring application-context.");
        }
      }
    } catch (NamingException e) {
      throw new RuntimeException("Could not find application context for spring EL resolver", e);
    }
  }

  public Class< ? > getCommonPropertyType(ELContext context, Object arg) {
    return Object.class;
  }

  public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object arg) {
    return null;
  }
  
  public Class< ? > getType(ELContext context, Object arg1, Object arg2) {
    return Object.class;
  }
  
  public class ContextWrapper {
    Object applicationContext;
    
    public ContextWrapper(Object applicationContext) {
      this.applicationContext = applicationContext;
    }
    
    public boolean containsBean(String key) {
      try {
        Method containsBean = applicationContext.getClass().getMethod("containsBean", String.class);
        return (Boolean) containsBean.invoke(applicationContext, key);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
    
    
    public Object getBean(String key) {
      try {
        Method containsBean = applicationContext.getClass().getMethod("getBean", String.class);
        return containsBean.invoke(applicationContext, key);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  };

}
