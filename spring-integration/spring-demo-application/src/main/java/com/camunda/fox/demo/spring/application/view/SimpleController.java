package com.camunda.fox.demo.spring.application.view;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.camunda.fox.demo.spring.application.dao.ProductDao;
import com.camunda.fox.demo.spring.application.model.Product;

@Controller
public class SimpleController {
	
	@Inject
	private ProcessEngine processEngine;
	
	@Inject
	private ProductDao productDao; 
		
	// combine both JPA and ProcessEngine work into a single UnitOfWork
	@Transactional	
	@RequestMapping("/simple")
	public @ResponseBody String simple() {
		
		Product product = new Product();
		productDao.storeNewProduct(product);
		
		Long productId = product.getId();
		Map<String, Object> processVariables = new HashMap<String, Object>();
		processVariables.put("productId", productId);
		
		ProcessInstance instance = processEngine
				.getRuntimeService()
				.startProcessInstanceByKey("testProcess",processVariables);
		
		return "ProcessInstnaceId: "+instance.getId() +"; ProductId:"+ productId;
	}

}
