package com.camunda.fox.demo.spring.application.process;

import javax.inject.Inject;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

import com.camunda.fox.demo.spring.application.dao.ProductDao;
import com.camunda.fox.demo.spring.application.model.Product;

/**
 * This is a Spring bean that is invoked by the ProcessEngine
 *
 */
@Service
public class TestBean implements JavaDelegate {

	@Inject
	private ProductDao productDao;

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		
		Long productId = (Long) execution.getVariable("productId");
		Product product = productDao.getProductById(productId);
		
		// automatically flushed at the end of transaction
		product.setIsProcessed(true);

	}

}
