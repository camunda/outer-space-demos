package com.camunda.fox.demo.spring.application.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.camunda.fox.demo.spring.application.model.Product;

@Component
public class ProductDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Product getProductById(Long id) {
		return entityManager.find(Product.class, id);
	}
	
	public void storeNewProduct(Product newProduct) {
		entityManager.persist(newProduct);
		entityManager.flush();
	}
	
	/**
	 * only required if you have a "detached" product-object.
	 * 
	 * @param product
	 */
	public void updateProduct(Product product) {
		entityManager.merge(product);
	}
}
