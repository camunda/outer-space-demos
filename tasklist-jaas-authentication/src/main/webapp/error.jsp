<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>camunda fox - task list - Sign in</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link type="text/css" rel="stylesheet" href="/tasklist/javax.faces.resource/css/bootstrap.css.jsf" />
  <style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>
  <link type="text/css" rel="stylesheet" href="/tasklist/javax.faces.resource/css/responsive.css.jsf" />

  <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>

  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container-fluid" style="padding: 0;">
        <a class="brand" href="#" style="margin:0 0 0 17px; padding:0;"><img
          src="/tasklist/resources/img/FoxTasklist_white.png"
          alt="camunda fox - task list" /></a>
      </div>
    </div>
  </div>

  <hr />
  <div class="container-fluid">
    <div id="content">
      <div class="row">
        <h1 class="span4 offset4">Login failed</h1>
      </div>
      <div class="row">
        <p>Could not login with the provided credentials. <a href="login.jsp">Try again.</a></p>
      </div>
    </div>


    <hr />

    <footer>
      <p>
        powered by <a href="http://www.camunda.com/fox/">camunda fox</a>
      </p>
    </footer>

  </div>
  <!--/.fluid-container-->

  <script src="#{resource['/tasklist/resource/js/jquery.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-transition.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-alert.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-modal.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-dropdown.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-scrollspy.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-tab.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-tooltip.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-popover.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-button.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-collapse.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-carousel.js']}"></script>
  <script src="#{resource['/tasklist/resource/js/bootstrap-typeahead.js']}"></script>

</body>
</html>
