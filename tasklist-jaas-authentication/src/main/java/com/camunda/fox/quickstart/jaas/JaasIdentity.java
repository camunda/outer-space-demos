package com.camunda.fox.quickstart.jaas;

import java.io.Serializable;
import java.security.Principal;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@Stateless
public class JaasIdentity implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @Resource 
  private SessionContext sessionContext;
 
  @Inject
  private Principal principal;
  
  public String getInfo() {
    boolean clerk = sessionContext.isCallerInRole("clerk");
    
    return principal.getName() + " is clerk = " + clerk;
  }

}
