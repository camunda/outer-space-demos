package com.camunda.fox.quickstart.interpocesscommunication.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@ApplicationScoped
@Named
public class ServiceRegistry {
  
  private Map<String, String> baseUrls = new HashMap<String, String>();
  private Map<String, String> wsdls = new HashMap<String, String>();
  
  public void register(String processDefinitionKey, String baseUrl, String wsdlLocation) {
    baseUrls.put(processDefinitionKey, baseUrl);
    wsdls.put(processDefinitionKey, wsdlLocation);
  }

  public String getBaseUrl(String processDefinitionKey) {
    if (baseUrls.containsKey(processDefinitionKey)) {
      return baseUrls.get(processDefinitionKey);
    } else {
      throw new RuntimeException("No base url registered for process '" + processDefinitionKey + "'.");
    }
}
  
  public URL getWsdlLocation(String processDefinitionKey) {
      if (wsdls.containsKey(processDefinitionKey)) {
        String wsdlLocation = wsdls.get(processDefinitionKey);
        try {
          return new URL(wsdlLocation);
        } catch (MalformedURLException e) {
          throw new RuntimeException("The URL '" + wsdlLocation + "' registered for process '" + processDefinitionKey + "' seems to be malformed.", e);
        }
      } else {
        throw new RuntimeException("No service registered for process '" + processDefinitionKey + "'.");
      }
  }
  
  public InterProcessCommunicationService getServiceRefernceToTalkToRemoteProcess(String processDefinitionKey) {
    return getServiceRefernce(getWsdlLocation(processDefinitionKey));
  }

  public InterProcessCommunicationService getServiceRefernce(URL wsdlLocation) {
    return new InterProcessCommunicationServiceService(wsdlLocation).getInterProcessCommunicationServicePort();
  }
}
