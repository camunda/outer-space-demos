package com.camunda.fox.quickstart.interpocesscommunication.ws;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.inject.Named;



@Startup
@Singleton
public class ServiceRegistryStarter {
  
  @Inject
  @Named
  private ServiceRegistry serviceRegistry;
    
  @PostConstruct
  public void registerServices() {
    serviceRegistry.register("inter-process-communication-ws-child", "http://localhost:18080/", "http://localhost:18080/inter-process-communication-ws/InterProcessCommunicationService?wsdl");
    serviceRegistry.register("inter-process-communication-ws-parent", "http://localhost:8080/", "http://localhost:8080/inter-process-communication-ws/InterProcessCommunicationService?wsdl");
  }

}
