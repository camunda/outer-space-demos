package com.camunda.fox.quickstart.interpocesscommunication.ws;


public class InterProcessCommunicationConstants {
  
  public static final String CURRENTLY_CALLED_PROCESS_DEFINITION_KEY = "currentlyCalledProcessDefinitionKey";  
  public static final String CALLBACK_URL = "callbackURL";
  
  public static final String CALLBACK_CORRELATION_ID = "callbackCorrelationId";
  public static final String CORRELATION_ID_PREFIX = "correlationIdForInvocationOf_";
  
  public static final String PAYLOAD = "payload";
  public static final String PAYLOAD_RECEIVED_FROM_CALLBACK = "payloadReceivedFromCallback";
  public static final String SAMPLE_PAYLOAD_PREFIX = "sample-payload-";

}
