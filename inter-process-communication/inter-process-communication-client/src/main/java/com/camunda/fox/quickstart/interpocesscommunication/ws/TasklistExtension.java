package com.camunda.fox.quickstart.interpocesscommunication.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;

@Named
public class TasklistExtension {
  
  @Inject
  private RuntimeService runtimeService;  

  /**
   * {@link ServiceRegistry} to ask for the wsdl / service reference
   * for a remote process engine
   */
  @Inject
  private ServiceRegistry serviceRegistry;  

  /**
   * TODO: This MUST be cached! Otherwise this is called multiple times when JSF renders the page and calling the WS multiple times!
   * So this is a performance bad practice.
   */
  public List<TaskDto> getTasksOfRemoteProcesses() {
    ArrayList<TaskDto> tasks = new ArrayList<TaskDto>();
    
    Map<String,List<String>> correlationIdsForProcessDefinitionKeys = new HashMap<String, List<String>>();
    
    List<Execution> waitingExecutions = runtimeService.createExecutionQuery()
      .variableValueLike(InterProcessCommunicationConstants.CURRENTLY_CALLED_PROCESS_DEFINITION_KEY, "%")
      .list();    
    
    // This is pretty inefficient, a better way would be to write a query returning all correlation ids at once grouped 
    // by process definition as we need them. This could be added as described here:
    // https://app.camunda.com/confluence/display/foxUserGuide/Performance+Tuning+with+custom+Queries
    for (Execution execution : waitingExecutions) {
      String processDefinitionKey = (String) runtimeService.getVariable(execution.getId(), InterProcessCommunicationConstants.CURRENTLY_CALLED_PROCESS_DEFINITION_KEY);
      String correlationId = (String) runtimeService.getVariable(execution.getId(), InterProcessCommunicationConstants.CORRELATION_ID_PREFIX + processDefinitionKey);
      
      if (!correlationIdsForProcessDefinitionKeys.containsKey(processDefinitionKey)) {
        correlationIdsForProcessDefinitionKeys.put(processDefinitionKey, new ArrayList<String>());
      }
      correlationIdsForProcessDefinitionKeys.get(processDefinitionKey).add(correlationId);
      
    }
    
    for (Entry<String, List<String>> correlationIdsForProcessDefinitionKey : correlationIdsForProcessDefinitionKeys.entrySet()) {      
      // get WSDL URL and prepare CXF client
      String processDefinitionKey = correlationIdsForProcessDefinitionKey.getKey();
      List<String> correlationIds = correlationIdsForProcessDefinitionKey.getValue();
      
      InterProcessCommunicationService service = serviceRegistry.getServiceRefernceToTalkToRemoteProcess(processDefinitionKey);

      tasks.addAll(
              service.getTasksForCorrelationIds(correlationIds));
    }

    return tasks;
  }

  
  public String getFormUrl(TaskDto task) {
    return serviceRegistry.getBaseUrl(task.getProcessDefinitionKey()) + task.getFormUrl() + "&callbackUrl=" + getCallbackUrl();    
  }
  
  public String getCallbackUrl() {
    Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
    if (request instanceof HttpServletRequest) {
      return ((HttpServletRequest) request).getRequestURL().toString();
    } else {
      return "";
    }
  }

}
