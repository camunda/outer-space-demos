package com.camunda.fox.quickstart.interpocesscommunication.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;

@Named("interProcessCommunication")
public class InterProcessCommunicationClient {

  @Inject
  private ServiceRegistry serviceRegistry;

  public void startRemoteProcessInstance(String processDefinitionKey, DelegateExecution execution) {
    InterProcessCommunicationService service = serviceRegistry.getServiceRefernceToTalkToRemoteProcess(processDefinitionKey);

    // generate callback URL and correlation ID
    // = "inter-process-communication-ws-parent"
    String thisProcessKey = ((ProcessDefinitionEntity) ((ExecutionEntity) execution).getProcessDefinition()).getKey();
    String callbackUrl = serviceRegistry.getWsdlLocation(thisProcessKey).toString();
    
    String correlationId = UUID.randomUUID().toString();

    // store correlation ID
    execution.setVariable(InterProcessCommunicationConstants.CORRELATION_ID_PREFIX + processDefinitionKey, correlationId);
    // and remember which process definition was called as sub process
    execution.setVariable(InterProcessCommunicationConstants.CURRENTLY_CALLED_PROCESS_DEFINITION_KEY, processDefinitionKey);

    // call service
    service.startProcessInstanceWithString(processDefinitionKey, callbackUrl, correlationId, InterProcessCommunicationConstants.SAMPLE_PAYLOAD_PREFIX + correlationId);
  }

  public void invokeProcessCallback(DelegateExecution execution, String payload) throws MalformedURLException {
    // lookup service URL
    URL wsdlLocation = new URL((String) execution.getVariable(InterProcessCommunicationConstants.CALLBACK_URL));

    InterProcessCommunicationService service = serviceRegistry.getServiceRefernce(wsdlLocation);

    // restore correlation information
    // TODO: MAke this generic
    //String calledProcess = "inter-process-communication-ws-child";
    String calledProcess = ((ProcessDefinitionEntity) ((ExecutionEntity) execution).getProcessDefinition()).getKey();
    
    String correlationId = (String) execution.getVariable(InterProcessCommunicationConstants.CALLBACK_CORRELATION_ID);
    
    // call web service
    service.invokeProcessCallbackWithPayloadString(calledProcess, correlationId, payload);
  }
}
