package com.camunda.fox.quickstart.interpocesscommunication.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jws.WebService;

import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.task.Task;

import com.camunda.fox.client.impl.ProcessArchiveSupport;
import com.camunda.fox.platform.api.ProcessArchiveService;
import com.camunda.fox.platform.spi.ProcessArchive;

/**
 * Deployed to http://localhost:8080/inter-process-communication-ws/InterProcessCommunicationService?wsdl
 */
@WebService(name = "InterProcessCommunicationService")
public class InterProcessCommunicationService {

  @Inject
  private TaskService taskService;
  
  @Inject
  private RuntimeService runtimeService;
  
  @Inject
  private RepositoryService repositoryService;
  
  @EJB(lookup = ProcessArchiveSupport.PROCESS_ARCHIVE_SERVICE_NAME)
  private ProcessArchiveService processArchiveService;
  
  @Inject
  private ProcessEngine processEngine;

  @Inject
  private FormService formService;  

  public void startProcessInstanceWithString(String processDefinitionKey, String callbackUrl, String correlationId, String payload) {
    Map<String, Object> variables = new HashMap<String, Object>();
    variables.put(InterProcessCommunicationConstants.CALLBACK_URL, callbackUrl);
    variables.put(InterProcessCommunicationConstants.CALLBACK_CORRELATION_ID, correlationId);
    variables.put(InterProcessCommunicationConstants.PAYLOAD, payload);

    runtimeService.startProcessInstanceByKey(processDefinitionKey, variables);
  }

  public void startProcessInstanceWithMap(String processDefinitionKey, String callbackUrl, String correlationId, ParameterMap parameterMap) {
    Map<String, Object> variables = new HashMap<String, Object>();
    variables.put(InterProcessCommunicationConstants.CALLBACK_URL, callbackUrl);
    variables.put(InterProcessCommunicationConstants.CALLBACK_CORRELATION_ID, correlationId);
    variables.putAll(parameterMap.getAsMap());

    runtimeService.startProcessInstanceByKey(processDefinitionKey, variables);
  }

  public void invokeProcessCallbackWithPayloadMap(String calledProcess, String correlationId, ParameterMap parameters) {
    signalProcessInstance(calledProcess, correlationId, parameters.getAsMap());
  }

  public void invokeProcessCallbackWithPayloadString(String calledProcess, String correlationId, String payload) {
    Map<String, Object> variables = new HashMap<String, Object>();
    variables.put(InterProcessCommunicationConstants.PAYLOAD_RECEIVED_FROM_CALLBACK, payload);
    signalProcessInstance(calledProcess, correlationId, variables);
  }

  private void signalProcessInstance(String calledProcess, String correlationId, Map<String, Object> variables) {
    Execution execution = runtimeService.createExecutionQuery()
            .variableValueEquals(InterProcessCommunicationConstants.CORRELATION_ID_PREFIX + calledProcess, correlationId).singleResult();

    // remove variable for process which is currently called
    variables.put(InterProcessCommunicationConstants.CURRENTLY_CALLED_PROCESS_DEFINITION_KEY, null);
    
    // TODO: Delete correlation

    runtimeService.signal(execution.getId(), variables);
  }  

  public List<TaskDto> getTasksForCorrelationIds(List<String> correlationIds) {
    ArrayList<TaskDto> tasks = new ArrayList<TaskDto>();

    // Better than the loop would be to introduce an own query doing this in one
    // SQL, including all necessary information, see
    // https://app.camunda.com/confluence/display/foxUserGuide/Performance+Tuning+with+custom+Queries
    for (String id : correlationIds) {
      List<Task> tasksForCorrelationId = taskService.createTaskQuery().processVariableValueEquals(InterProcessCommunicationConstants.CALLBACK_CORRELATION_ID, id).list();
      for (Task task : tasksForCorrelationId) {
        TaskDto dto = new TaskDto();
        dto.setId(task.getId());
        dto.setName(task.getName());
        dto.setDescription(task.getDescription());
        dto.setCreated(task.getCreateTime());
        dto.setAssignee(task.getAssignee());        
        dto.setExecutionId(task.getExecutionId());
        dto.setProcessInstanceId(task.getProcessInstanceId());
        
        dto.setFormUrl(getTaskFormUrl(task));
        
        dto.setProcessDefinitionId(task.getProcessDefinitionId());
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).singleResult();
        dto.setProcessDefinitionName(processDefinition.getName());
        dto.setProcessDefinitionKey(processDefinition.getKey());
        
        tasks.add(dto);
      }
    }
    return tasks;
  }

  /**
   * TODO: This could be maybe moved to the platform-client?
   */
  private String getTaskFormUrl(Task task) {
    TaskFormData taskFormData = formService.getTaskFormData(task.getId());
    if (taskFormData == null || taskFormData.getFormKey() == null) {
      return null;
    } else {
      return getFormUrl(task.getProcessDefinitionId(), taskFormData.getFormKey(), "taskId=" + task.getId());
    }
  }
  
  private String getFormUrl(String processDefinitionId, String formKey, String urlParameters) {
    try {
      ProcessArchive processArchive = processArchiveService.getProcessArchiveByProcessDefinitionId(processDefinitionId, processEngine.getName());
      String contextPath = (String) processArchive.getProperties().get(ProcessArchive.PROP_SERVLET_CONTEXT_PATH);
      return contextPath + "/" + formKey + ".jsf?" + urlParameters;
    } catch (Exception ex) {
      ex.printStackTrace();
//      log.log(Level.INFO, "Could not resolve context path for process definition " + processDefinitionId, ex);
      return null;
    }
  }

}
