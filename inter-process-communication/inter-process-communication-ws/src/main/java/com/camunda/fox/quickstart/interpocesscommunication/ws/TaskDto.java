package com.camunda.fox.quickstart.interpocesscommunication.ws;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taskDto")
public class TaskDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id;
  private String name;
  private String description;
  private String assignee;
  private Date created;
  private String processInstanceId;
  private String executionId;
  private String processDefinitionId;
  private String processDefinitionKey;
  private String processDefinitionName;

  private String formUrl;

  public TaskDto() {
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public String getAssignee() {
    return assignee;
  }

  public String getProcessInstanceId() {
    return processInstanceId;
  }

  public String getExecutionId() {
    return executionId;
  }

  public String getProcessDefinitionId() {
    return processDefinitionId;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public String getProcessDefinitionKey() {
    return processDefinitionKey;
  }

  public void setProcessDefinitionKey(String processDefinitionKey) {
    this.processDefinitionKey = processDefinitionKey;
  }

  public String getProcessDefinitionName() {
    return processDefinitionName;
  }

  public void setProcessDefinitionName(String processDefinitionName) {
    this.processDefinitionName = processDefinitionName;
  }

  public String getFormUrl() {
    return formUrl;
  }

  public void setFormUrl(String formUrl) {
    this.formUrl = formUrl;
  }

  
  public void setId(String id) {
    this.id = id;
  }

  
  public void setName(String name) {
    this.name = name;
  }

  
  public void setDescription(String description) {
    this.description = description;
  }

  
  public void setAssignee(String assignee) {
    this.assignee = assignee;
  }

  
  public void setProcessInstanceId(String processInstanceId) {
    this.processInstanceId = processInstanceId;
  }

  
  public void setExecutionId(String executionId) {
    this.executionId = executionId;
  }

  
  public void setProcessDefinitionId(String processDefinitionId) {
    this.processDefinitionId = processDefinitionId;
  }

  @Override
  public String toString() {
    return "TaskDto [id=" + id + ", name=" + name + ", description=" + description + ", assignee=" + assignee + ", created=" + created + ", processInstanceId="
            + processInstanceId + ", executionId=" + executionId + ", processDefinitionId=" + processDefinitionId + ", processDefinitionKey="
            + processDefinitionKey + ", processDefinitionName=" + processDefinitionName + ", formUrl=" + formUrl + "]";
  }

}
