package com.camunda.fox.quickstart.interpocesscommunication.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ParameterMap")
@XmlAccessorType(XmlAccessType.FIELD)
public class ParameterMap {

  @XmlElement(nillable = false, name = "entry")
  private List<ParamaterEntry> entries = new ArrayList<ParamaterEntry>();

  public List<ParamaterEntry> getEntries() {
    return entries;
  }
  
  public Map<String, Object> getAsMap() {
    HashMap<String, Object> map = new HashMap<String, Object>();
    for (ParamaterEntry entry : getEntries()) {
      map.put(entry.getKey(), entry.getValue());
    }
    return map;
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "ParamaterEntry")
  static class ParamaterEntry {

    // Map keys cannot be null
    @XmlElement(required = true, nillable = false)
    String key;

    String value;

    public String getKey() {
      return key;
    }

    public void setKey(String key) {
      this.key = key;
    }

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;
    }

  }
}