# Introduction
This project shows how to dynamically switch to Asynchronous Continuation inside
a service task.

See also: https://app.camunda.com/confluence/display/foxUserGuide/Asynchronous+Continuation+on+Error

# Environment Restrictions

# Remarks to run this application
There is no web interface to access the application. To get started refer to the
Arquillian test case, which by default connects to a fox platform running
locally on JBoss AS 7.

# Known Issues
- This approach does not work correctly for service tasks with attached boundary
  events that make the service task become a scope, e.g., timer events. However,
  error boundary events do work.
- The business logic must not mark the transaction for rollback only, because
  otherwise the Asynchronous Continuation job can not be committed to the
  database and the process instance will get stuck. Even if a job could be
  committed, it would most likely start at the wrong activity.

# Improvements Backlog
