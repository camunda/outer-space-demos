package org.example;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.impl.bpmn.behavior.BpmnActivityBehavior;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.jobexecutor.AsyncContinuationJobHandler;
import org.activiti.engine.impl.persistence.entity.MessageEntity;
import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;


public class DynamicAsynchronousContinuationDelegate implements ActivityBehavior {

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		if (null == getState(execution)) {
			setState(execution, "STATE 1");
			createAsynchronousContinuationJob(execution);
		} else if ("STATE 1".equals(getState(execution))) {
			setState(execution, "STATE 2");
			createAsynchronousContinuationJob(execution);
		} else if ("STATE 2".equals(getState(execution))) {
			setState(execution, "STATE 3");
			createAsynchronousContinuationJob(execution);
		} else if ("STATE 3".equals(getState(execution))) {
			new BpmnActivityBehavior().performDefaultOutgoingBehavior(execution);
		}
	}

	private void setState(ActivityExecution execution, String stateName) {
		execution.setVariable(getStateVariableName(execution), stateName);
	}

	private String getState(ActivityExecution execution) {
		return (String) execution.getVariable(getStateVariableName(execution));
	}

	private String getStateVariableName(ActivityExecution execution) {
		return execution.getActivity().getId() + "_STATE";
	}
	
	private void createAsynchronousContinuationJob(DelegateExecution execution) {
		MessageEntity message = new MessageEntity();
		message.setProcessInstanceId(execution.getProcessInstanceId());
		message.setExecutionId(execution.getId());
	    message.setExclusive(true);
	    message.setJobHandlerType(AsyncContinuationJobHandler.TYPE);

	    Context
	      .getCommandContext()
	      .getJobManager()
	      .send(message);
	}

}
