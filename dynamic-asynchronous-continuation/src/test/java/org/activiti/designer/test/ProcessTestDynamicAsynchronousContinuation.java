package org.activiti.designer.test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.util.LogUtil;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.test.Deployment;
import org.activiti.engine.test.ActivitiRule;
import org.junit.Rule;
import org.junit.Test;

public class ProcessTestDynamicAsynchronousContinuation {

	static {
		LogUtil.readJavaUtilLoggingConfigFromClasspath();
	}

	@Rule
	public ActivitiRule activitiRule = new ActivitiRule();

	@Test
	@Deployment(resources = "diagrams/DynamicAsynchronousContinuation.bpmn20.xml")
	public void startProcess() {
		RuntimeService runtimeService = activitiRule.getRuntimeService();
		Map<String, Object> variableMap = new HashMap<String, Object>();
		variableMap.put("name", "Activiti");
		ProcessInstance processInstance = runtimeService
				.startProcessInstanceByKey("DynamicAsynchronousContinuation",
						variableMap);
		String pid = processInstance.getId();
		assertNotNull(pid);
		System.out.println("id " + pid + " "
				+ processInstance.getProcessDefinitionId());
		
		executeJob(pid);
		executeJob(pid);
		executeJob(pid);
		
		assertTrue(isProcessEnded(pid));
	}

	private void executeJob(String pid) {
		Job job = activitiRule.getManagementService().createJobQuery().singleResult();
		assertNotNull(job);
		assertEquals(pid, job.getProcessInstanceId());
		assertFalse(isProcessEnded(pid));
		activitiRule.getManagementService().executeJob(job.getId());
	}

	private boolean isProcessEnded(String pid) {
		if (null == activitiRule.getRuntimeService().createProcessInstanceQuery().processInstanceId(pid).singleResult()) {
			HistoricProcessInstance historicProcessInstance = activitiRule.getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(pid).singleResult();
			if (null != historicProcessInstance.getEndTime()
					&& "endevent1".equals(historicProcessInstance.getEndActivityId())) {
				return true;
			}
		}
		return false;
	}
}