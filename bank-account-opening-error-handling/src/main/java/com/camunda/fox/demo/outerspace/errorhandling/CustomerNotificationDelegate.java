package com.camunda.fox.demo.outerspace.errorhandling;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.HistoryService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.history.HistoricActivityInstance;
import org.apache.commons.lang.time.DateUtils;

@Named
public class CustomerNotificationDelegate implements JavaDelegate {

  @Inject
  private HistoryService historyService;

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    simulateServiceOutage(execution);
    System.out.println("The Customer has been informed.");
  }

  private void simulateServiceOutage(DelegateExecution execution) throws IOException {
    List<HistoricActivityInstance> previousActivities = historyService
      .createHistoricActivityInstanceQuery()
      .processInstanceId(execution.getProcessInstanceId())
      .finished()
      .list();
    Date endTimeOfPreviousActivity = previousActivities.get(previousActivities.size() - 1).getEndTime();
    if (DateUtils.addSeconds(endTimeOfPreviousActivity, 30).after(new Date())) {
      throw new IOException("Customer Notification Service unreachable");
    }
  }

}
