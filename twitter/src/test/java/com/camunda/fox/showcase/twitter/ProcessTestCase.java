package com.camunda.fox.showcase.twitter;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;
import org.junit.Assert;
import org.junit.matchers.JUnitMatchers;

public class ProcessTestCase extends ActivitiTestCase {

  @Deployment(resources = "TwitterDemoProcess.bpmn")
  public void testDeployment() {
  }
	  
  @Deployment(resources = "TwitterDemoProcess.bpmn")
  /**
   * Disabled test case, enable it by rename it to "testApprovedPath".
   * Disabled because this a bad example of unit testing since we call Twitter here without
   * providing a mock service ;-)
   */
  public void notestApprovedPath() {
    Map<String, Object> variables = new HashMap<String, Object>();
    variables.put("content", "Hello from JUnit test case at system time: "+System.currentTimeMillis()+"!");
    
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("TwitterDemoProcess", variables);
    String id = processInstance.getId();
    System.out.println("Started process instance id " + id);
    
    Assert.assertThat(runtimeService.getActiveActivityIds(id), JUnitMatchers.hasItem("Review_Tweet_3"));

    Task task = taskService.createTaskQuery().taskAssignee("kermit").singleResult();
    variables.put("approved", Boolean.TRUE);
    taskService.complete(task.getId(), variables);
    
    assertProcessEnded(id);

    HistoricProcessInstance historicProcessInstance = historicDataService.createHistoricProcessInstanceQuery().processInstanceId(id).singleResult();
    assertNotNull(historicProcessInstance);

    System.out.println("Finished, took " + historicProcessInstance.getDurationInMillis() + " millis");
  }
  
  @Deployment(resources = "TwitterDemoProcess.bpmn")  
  public void testRejectedPath() {
    Map<String, Object> variables = new HashMap<String, Object>();
    variables.put("content", "We will never see this content on Twitter");
    variables.put("email", "bernd.ruecker@camunda.com");
    
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("TwitterDemoProcess", variables);
    String id = processInstance.getId();
    System.out.println("Started process instance id " + id);
    
    Assert.assertThat(runtimeService.getActiveActivityIds(id), JUnitMatchers.hasItem("Review_Tweet_3"));

    Task task = taskService.createTaskQuery().taskAssignee("kermit").singleResult();
    variables.put("approved", Boolean.FALSE);
    variables.put("comments", "No, we will not publish this on Twitter");
    taskService.complete(task.getId(), variables);
    
    assertProcessEnded(id);

    HistoricProcessInstance historicProcessInstance = historicDataService.createHistoricProcessInstanceQuery().processInstanceId(id).singleResult();
    assertNotNull(historicProcessInstance);

    System.out.println("Finished, took " + historicProcessInstance.getDurationInMillis() + " millis");
  }
  
  /**
   * Disabled (used to showcase Activiti API)
   */
  public void notestStandalone() {
    // deploy process definition
    ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    RepositoryService repositoryService = processEngine.getRepositoryService();
    DeploymentBuilder deployment = repositoryService.createDeployment();
    deployment.addClasspathResource("TwitterDemoProcess.bpmn");
    deployment.deploy();
   
    // create process variables
    Map<String, Object> variables = new HashMap<String, Object>();
    variables.put("Tweet", "Testing Activiti 5");
    variables.put("Email", "info@camunda.com");

    // start a new process instance
    RuntimeService runtimeService = processEngine.getRuntimeService();
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("TwitterDemoProcess", variables);

    // test
    String id = processInstance.getId();
    System.out.println("Started process instance id " + id);
    Assert.assertThat(runtimeService.getActiveActivityIds(id), JUnitMatchers.hasItem("Review_Tweet_3"));

  }
}