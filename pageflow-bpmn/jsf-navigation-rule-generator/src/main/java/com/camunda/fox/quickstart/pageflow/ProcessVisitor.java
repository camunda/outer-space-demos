package com.camunda.fox.quickstart.pageflow;

import org.eclipse.bpmn2.Activity;
import org.eclipse.bpmn2.EndEvent;
import org.eclipse.bpmn2.StartEvent;

/**
 *
 * @author nico.rehwaldt
 */
public interface ProcessVisitor {

  public void start(StartEvent e);
  
  public void visit(Activity activity);
  
  public void end(EndEvent e);
}
