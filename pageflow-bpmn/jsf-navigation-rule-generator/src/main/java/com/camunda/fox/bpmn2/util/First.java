package com.camunda.fox.bpmn2.util;

import java.util.List;

/**
 *
 * @author nico.rehwaldt
 */
public class First {

  public static <T> T byType(Class<T> cls, List<?> elements) {
    for (Object o: elements) {
      if (cls.isInstance(o)) {
        return cls.cast(o);
      }
    }
    
    return null;
  }
}
