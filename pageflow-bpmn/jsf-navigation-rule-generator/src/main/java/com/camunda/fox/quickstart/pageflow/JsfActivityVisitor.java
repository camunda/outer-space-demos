package com.camunda.fox.quickstart.pageflow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.bpmn2.Activity;
import org.eclipse.bpmn2.CallActivity;
import org.eclipse.bpmn2.EndEvent;
import org.eclipse.bpmn2.StartEvent;
import org.eclipse.bpmn2.UserTask;
import org.eclipse.bpmn2.impl.ProcessImpl;
import org.mvel2.templates.TemplateRuntime;

/**
 * 
 * @author nico.rehwaldt
 */
public class JsfActivityVisitor implements ProcessVisitor {

  private final PrintWriter writer;

  public JsfActivityVisitor(PrintWriter writer) {
    this.writer = writer;
  }

  public void visit(Activity activity) {
    if (activity instanceof UserTask || activity instanceof CallActivity) {
      Map<String, Object> variables = new HashMap<String, Object>();
      variables.put("activity", activity);
      String rule = (String) TemplateRuntime.eval(getTemplate("navigation-rule.tpl"), variables);
      writer.append(rule);
    } else {
      writer.append("\n  <!-- no rule for Activity[" + activity.getId() + "] of type " + activity.getClass().getSimpleName() + " -->\n");
    }
  }

  public void start(StartEvent e) {
    writer.println("\n  <!-- Navigation rules for \"" + ((ProcessImpl) e.eContainer()).getName() + "\" -->");
  }

  public void end(EndEvent e) {
    writer.println("  <!-- // Navigation rules for \"" + ((ProcessImpl) e.eContainer()).getName() + "\" -->\n");
  }

  private Map<String, String> fileCache = new HashMap<String, String>();

  private String getTemplate(String name) {
    String template = fileCache.get(name);
    if (template == null) {
      template = readTemplate(name);
      fileCache.put(name, template);
    }

    return template;
  }

  private String readTemplate(String name) {
    StringWriter contentsCapture = new StringWriter();

    try {
      InputStream is = getClass().getResourceAsStream(name);
      BufferedReader reader = new BufferedReader(new InputStreamReader(is));

      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }

        contentsCapture.write(line);
        contentsCapture.write("\n");
      }
    } catch (IOException e) {
      throw new RuntimeException("Failed to read template " + name, e);
    }

    return contentsCapture.toString();
  }
}
