package com.camunda.fox.quickstart.pageflow;

import com.camunda.fox.bpmn2.util.Bpmn2Extensions;
import com.camunda.fox.bpmn2.util.Filter;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.bpmn2.Definitions;
import org.eclipse.bpmn2.DocumentRoot;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 *
 * @author nico.rehwaldt
 */
public class PageflowUtil {

  //////////////////////////////
  // Static helper methods    //
  //////////////////////////////
  
  public static List<org.eclipse.bpmn2.Process> extractPageFlows(org.eclipse.emf.ecore.resource.Resource resource) {
    
    EList<EObject> contents = resource.getContents();
    
    if (!contents.isEmpty()) {
      // assume one root element
      EObject root = contents.get(0);
      if (root instanceof DocumentRoot) {
        return extractPageFlows((DocumentRoot) root);
      }
    }
    
    return Collections.<org.eclipse.bpmn2.Process>emptyList();
  }
  
  public static List<org.eclipse.bpmn2.Process> extractPageFlows(DocumentRoot root) {
    return extractPageFlows((Definitions) root.eContents().get(0));
  }
  
  public static List<org.eclipse.bpmn2.Process> extractPageFlows(Definitions definitions) {
    List<org.eclipse.bpmn2.Process> processes = Filter.byType(org.eclipse.bpmn2.Process.class, definitions.getRootElements());
    
    Iterator<org.eclipse.bpmn2.Process> iterator = processes.iterator();
    while (iterator.hasNext()) {
      org.eclipse.bpmn2.Process next = iterator.next();
      if (!isPageFlow(next)) {
        iterator.remove();
      }
    }
    
    return processes;
  }
  
  public static boolean isPageFlow(org.eclipse.bpmn2.Process process) {
    Map<String, Object> attributes = Bpmn2Extensions.filterAttributes(process, "http://camunda.com/fox", null);
    return attributes.containsKey("page-flow");
  }
}
