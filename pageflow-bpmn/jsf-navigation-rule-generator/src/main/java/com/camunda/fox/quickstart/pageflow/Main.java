package com.camunda.fox.quickstart.pageflow;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.bpmn2.Process;
import org.eclipse.bpmn2.util.Bpmn2ResourceImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;


/**
 *
 * @author nico.rehwaldt
 */
public class Main {

  public static void main(String[] args) throws IOException {
    
    if (args.length != 2) {
      System.out.println("Usage generator [bpmn-input-file] [output-file]");
      System.exit(1);
    }
    
    new Main().loadBpmnFile(args[0]).generatePageFlows(args[1]);
  }

  private List<Resource> resources;
  
  public Main loadBpmnFile(String fileName) throws IOException {
    resources.add(loadResource(fileName));    
    return this;
  }
  
  private Resource loadResource(String fileName) throws IOException {
    Resource r = new Bpmn2ResourceImpl(URI.createFileURI("pageflow-tmp.bpmn"));
    r.load(new FileInputStream(fileName), null);    
    return r;
  }  
  
  public Main generatePageFlows(String fileName) throws IOException {
    List<Process> pageFlows = new ArrayList<Process>();
    for (Resource resource : resources) {
      pageFlows.addAll(PageflowUtil.extractPageFlows(resource));      
    }
    
    OutputStream out = new FileOutputStream(fileName);    
    PrintWriter writer = new PrintWriter(out);
    
    PageflowGenerator generator = new PageflowGenerator(pageFlows);
    generator.generateUsing(new JsfActivityVisitor(writer));
    
    writer.close();
    out.close();
    
    return this;
  }
  
}
