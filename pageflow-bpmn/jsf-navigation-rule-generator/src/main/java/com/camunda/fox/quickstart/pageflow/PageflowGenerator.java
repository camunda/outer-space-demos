package com.camunda.fox.quickstart.pageflow;

import java.util.List;
import java.util.logging.Logger;

import org.eclipse.bpmn2.Activity;
import org.eclipse.bpmn2.EndEvent;
import org.eclipse.bpmn2.FlowElement;
import org.eclipse.bpmn2.Process;
import org.eclipse.bpmn2.StartEvent;

import com.camunda.fox.bpmn2.util.First;

/**
 *
 * @author nico.rehwaldt
 */
public class PageflowGenerator {
  
  private final static Logger log = Logger.getLogger(PageflowGenerator.class.getCanonicalName());
  
  private final List<Process> processes;
  
  public PageflowGenerator(List<Process> processes) {
    this.processes = processes;
  }
  
  public void generateUsing(ProcessVisitor visitor) {
    for (Process process: processes) {
      generateSinglePageFlow(process, visitor);
    }
  }
  
  protected void fail(String msg, Object ... args) {
    throw new IllegalArgumentException(String.format(msg, args));
  }

  private void generateSinglePageFlow(Process process, ProcessVisitor visitor) {
    
    log.fine("Generating page flow for Process[" + process.getId() + "]");
    
    // TODO: Validation? What to validate?
    StartEvent startEvent = First.byType(StartEvent.class, process.getFlowElements());
    if (startEvent == null) {
      fail("No start event found in process %s#%s", new Object[] { process.getName(), process.getId() });
    }
    
    // TODO: Validation? What to validate?
    EndEvent endEvent = First.byType(EndEvent.class, process.getFlowElements());
    if (endEvent == null) {
      fail("No end event found in process %s#%s", new Object[] { process.getName(), process.getId() });
    }
    
    visitor.start(startEvent);
    
    for (FlowElement e: process.getFlowElements()) {
      validate(e);
      
      if (e instanceof Activity) {
        visitor.visit((Activity) e);
      }
    }
    
    visitor.end(endEvent);
  }
  
  public void validate(FlowElement flowElement) {
    // TODO: Validate flow node elements before passing them to visitors
  }
}
