
  <!-- @{activity.name} -->
  <navigation-rule>
    <from-view-id>/@{((org.eclipse.bpmn2.impl.ProcessImpl)activity.eContainer()).id}/@{activity.id}.xhtml</from-view-id>
    @foreach{flow : activity.outgoing}
      <navigation-case>
        @comment{ ## START PROCESS INSTANCE IF THE NEXT TASK IS A SERVICE TASK ## }
        @if{flow.targetRef is org.eclipse.bpmn2.ServiceTask}
          @foreach{attribute : ((org.eclipse.bpmn2.ServiceTask) flow.getTargetRef()).getAnyAttribute()}
            @if{attribute.eStructuralFeature is org.eclipse.emf.ecore.impl.EAttributeImpl && ((org.eclipse.emf.ecore.impl.EAttributeImpl) attribute.eStructuralFeature).name.equalsIgnoreCase("expression")}
              <from-action>#{pageflowBean.execute}</from-action>
              <from-outcome>SERVICE-TASK:@{attribute.value.toString()}</from-outcome>
            @end{}
          @end{}
        @comment{ ## COMPLETE TASK AND REDIRECT TO TASKLIST IF NEXT ELEMENT IS AN END EVENT ## }
        @elseif{flow.targetRef is org.eclipse.bpmn2.EndEvent}
          <from-action>#{pageflowBean.execute}</from-action>
          <from-outcome>END-EVENT</from-outcome>
        @comment{ ## IF NONE OF THE ABOVE IS TRUE, USE STANDARD IMPLICIT NAVIGATION RULES WITH FROM-OUTCOME AND TO-VIEW-ID  ## }
        @else{}
          <from-outcome>@{flow.targetRef.name}</from-outcome>
	    @end{}
        <to-view-id>/@{((org.eclipse.bpmn2.impl.ProcessImpl)activity.eContainer()).id}/@{flow.targetRef.id}.xhtml</to-view-id>
        <redirect />
      </navigation-case>
	@end{}
	@comment{ ## REDIRECT TO CALLED ELEMENT IF CURRENT ACTIVITY IS A CALL-ACTIVITY ## }
    @if{activity is org.eclipse.bpmn2.CallActivity}
      <navigation-case>
        <from-action>#{pageflowBean.execute}</from-action>
        <from-outcome>CALL-ACTIVITY:@{((org.eclipse.bpmn2.impl.CallableElementImpl)((org.eclipse.bpmn2.CallActivity)activity).getCalledElementRef()).eProxyURI().fragment()}</from-outcome>
        <to-view-id>/@{((org.eclipse.bpmn2.impl.ProcessImpl)activity.eContainer()).id}/@{activity.id}.xhtml</to-view-id>
      </navigation-case>
    @end{}
  </navigation-rule>
