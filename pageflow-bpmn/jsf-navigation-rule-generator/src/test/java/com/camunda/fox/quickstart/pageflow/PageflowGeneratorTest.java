package com.camunda.fox.quickstart.pageflow;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import org.eclipse.bpmn2.util.Bpmn2ResourceFactoryImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.Test;
import org.eclipse.bpmn2.Process;

import static org.junit.Assert.*;

/**
 *
 * @author nico.rehwaldt
 */
public class PageflowGeneratorTest {
  
  @Test
  public void shouldPerformBasicallyCorrect() throws Exception {
    // create the ecore resource
    Resource resource = createResource("test-tmp.bpmn");
    resource.load(getResourceAsStream("ProcessPageflowSeparation.bpmn"), Collections.EMPTY_MAP);
    
    // extract only the processes which are page-flows, recognized by an attribute:
    // <process id="pageflow-start-user-process" name="Pageflow Start User Process" fox:page-flow="true">
    List<Process> pageFlows = PageflowUtil.extractPageFlows(resource);
    assertEquals(3, pageFlows.size());
    
    
    StringWriter outputCapture = new StringWriter();    
    outputCapture.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
    		"<faces-config version=\"2.0\" xmlns=\"http://java.sun.com/xml/ns/javaee\"\n" +
    		"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
    		"  xsi:schemaLocation=\"\n" +
    		"  http://java.sun.com/xml/ns/javaee\n" +
    		"  http://java.sun.com/xml/ns/javaee/web-facesconfig_2_0.xsd\">");
    
    PageflowGenerator generator = new PageflowGenerator(pageFlows);
    generator.generateUsing(new JsfActivityVisitor(new PrintWriter(outputCapture)));
    
    outputCapture.write("</faces-config>\n");    
    /**
     * TODO for pageflow engine (among others)
     *  * fix implementation in sample diagram
     *  * add conditions to diagram
     */
    System.out.println(outputCapture.toString());
  }
  
  //////////////////////////////
  // Helper methods           //
  //////////////////////////////
  
  protected Resource createResource(String name) throws URISyntaxException {
    URI uri = URI.createURI(name);
    return new Bpmn2ResourceFactoryImpl().createResource(uri);
  }
  
  protected InputStream getResourceAsStream(String name) {
    return getClass().getClassLoader().getResourceAsStream(name);
  }
}
