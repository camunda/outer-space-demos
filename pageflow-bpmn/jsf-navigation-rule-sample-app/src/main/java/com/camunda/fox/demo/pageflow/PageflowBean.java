package com.camunda.fox.demo.pageflow;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.NavigationCase;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.resource.spi.IllegalStateException;
import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;

@Named
//TODO: Conversation Scoped
@SessionScoped
public class PageflowBean implements Serializable {

  private static final Logger log = Logger.getLogger(PageflowBean.class.getCanonicalName());

  private static final long serialVersionUID = 1L;

  // TODO: remove...
  private String someString;

  private String callbackUrl;

  private Stack<String> callBackUrlBackup = new Stack<String>();

  private String taskId;

  @Inject
  private RuntimeService runtimeService;

  @Inject
  private TaskService taskService;

  public String getTaskId() {
    return taskId;
  }

  public void setTaskId(String taskId) {
    this.taskId = taskId;
  }

  public String getSomeString() {
    return someString;
  }

  public void setSomeString(String someString) {
    log.fine("setting someString to " + someString);
    this.someString = someString;
  }

  public void setCallbackUrl(String callbackUrl) {
    if (callbackUrl != null) {
      log.fine("Setting callbackUrl to " + callbackUrl);
      this.callBackUrlBackup.push( this.callbackUrl );
      this.callbackUrl = callbackUrl;
    } else {
      log.fine("setCallbackUrl called with null value, ignoring!");
    }
  }

  public String getCallbackUrl() {
    return callbackUrl;
  }

  private String getRequestURL() {
    Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
    if (request instanceof HttpServletRequest) {
      return ((HttpServletRequest) request).getRequestURL().toString();
    } else {
      return "";
    }
  }

  public void execute(String expression) throws IOException, IllegalStateException {
    if (expression.startsWith("CALL-ACTIVITY:")) {
      String calledElement = expression.split(":")[1];
      String redirectUrl = "../" + calledElement;
      log.fine("Redirecting to called element '" + calledElement + "' with redirect URL '" + redirectUrl + "'");
      setCallbackUrl(getRequestURL());
      FacesContext.getCurrentInstance().getExternalContext().redirect(redirectUrl);
    } else if (expression.startsWith("END-EVENT")) {
      if (taskId != null) {
        log.fine("Attempting to complete task with id '" + taskId + "'");
        taskService.complete(taskId);
        log.fine("Completed task with id '" + taskId + "'");
        log.fine("Redirecting to '" + callbackUrl + "'");
        FacesContext.getCurrentInstance().getExternalContext().redirect(callbackUrl);
      } else {
        log.fine("TaskId not set, assuming to be returning from callActivity. Resetting callbackUrl to '" + this.callBackUrlBackup.lastElement());
        String tmpCallbackUrl = callbackUrl;
        this.callbackUrl = this.callBackUrlBackup.pop();
        log.fine("Redirecting to '" + tmpCallbackUrl + "'");
        FacesContext.getCurrentInstance().getExternalContext().redirect(tmpCallbackUrl);
      }
    } else if (expression.startsWith("SERVICE-TASK:")) {
      String key = expression.split(":")[1];
      log.fine("Attempting to start process with key '" + key + "'");
      ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(key);
      log.fine("Started process with instance id '" + processInstance.getId() + "'");
      FacesContext.getCurrentInstance().getExternalContext().redirect(callbackUrl);
    } else {
      throw new IllegalStateException("Something went wrong, expression should start with one of 'CALL-ACTIVITY:', 'END-EVENT' or 'SERVICE-TASK:' but was '"
              + expression + "'");
    }
  }

  /**
   * This method is only needed to submit the form and set the values of the
   * form fields to the respective beans. It just returns the fromOutcome value
   * extracted from the navigation case.
   */
  public String next(String fromOutcome) {
    return fromOutcome;
  }

  public List<NavigationCase> getNavigationCases() {
    String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
    ConfigurableNavigationHandler configurableNavigationHandler = (ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication()
            .getNavigationHandler();
    Set<NavigationCase> navigationCases = configurableNavigationHandler.getNavigationCases().get(viewId);
    if (navigationCases != null) {
      return new ArrayList<NavigationCase>(navigationCases);
    }
    return null;
  }

}
