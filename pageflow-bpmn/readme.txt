This tutorial shows how to use BPMN 2.0 diagrams to model not only executable processes or completely manual processes but as well page-flows. It transforms the page-flows into JSF navigation rules.

See https://app.camunda.com/confluence/display/foxUserGuide/Generate+Pageflows+from+BPMN+2.0+Intro for details and installation information.