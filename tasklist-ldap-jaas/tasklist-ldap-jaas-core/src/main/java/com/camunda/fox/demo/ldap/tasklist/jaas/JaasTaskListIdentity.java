package com.camunda.fox.demo.ldap.tasklist.jaas;

import java.security.Principal;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Specializes;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.camunda.fox.tasklist.api.TaskListIdentity;
import com.camunda.fox.tasklist.api.TasklistUser;
import com.camunda.fox.tasklist.event.SignOutEvent;

/**
 * Exchange the default TaskListIdentity with a version using the 
 * current Principal (from JAAS)
 */
@Specializes
public class JaasTaskListIdentity extends TaskListIdentity {
  
  private static final long serialVersionUID = 1L;
  
  @Inject
  private Principal principal;
  
  @Inject
  private Event<SignOutEvent> signOutEvent;
  
  public TasklistUser getCurrentUser() {
    // TODO: Query name from LDAP?
    return new TasklistUser(principal.getName(), "", "");
  }

  public void setCurrentUser(TasklistUser currentUser) {
    // we use the user from JAAS, so ignore the setter
  }

  public String signOut() {
    // invalidate session (hence we will need a new JAAS principal/login)
    FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    signOutEvent.fire(new SignOutEvent());
    
    // and redirect to the start page (JAAS will intercept)
    return "taskList.jsf?faces-redirect=true";
  }

  public void signIn() {
    // no sign-in needed, if we arrived here JAAS has logged us on
  }
  
  public boolean isSignedIn() {
    // JAAS verifies for us that the user is already logged in
    return true;
  }
}
