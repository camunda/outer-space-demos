package com.camunda.fox.demo.ldap.tasklist.ldap.impl;

import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;

import com.camunda.fox.demo.ldap.tasklist.ldap.LdapConfiguration;

public abstract class LdapCommand<T> {

  public abstract T execute(DirContext ctx, LdapConfiguration ldapConfig) throws Exception;
  
  protected SearchControls getSimpleSearchControls() {
    SearchControls searchControls = new SearchControls();
    searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
    searchControls.setTimeLimit(30000);
    return searchControls;
  }
}
