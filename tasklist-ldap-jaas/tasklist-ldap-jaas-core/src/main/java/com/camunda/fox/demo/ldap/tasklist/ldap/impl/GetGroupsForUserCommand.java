package com.camunda.fox.demo.ldap.tasklist.ldap.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchResult;

import com.camunda.fox.demo.ldap.tasklist.ldap.LdapConfiguration;
import com.camunda.fox.tasklist.api.TaskListGroup;

public class GetGroupsForUserCommand extends LdapCommand<List<TaskListGroup>> {

  private String userId;

  public GetGroupsForUserCommand(String userId) {
    this.userId = userId;
  }

  @Override
  public List<TaskListGroup> execute(DirContext ctx, LdapConfiguration ldapConfig) throws Exception {
    List<TaskListGroup> groupIds = new ArrayList<TaskListGroup>();

    String dn = new GetDnForUserCommand(userId).execute(ctx, ldapConfig);

    NamingEnumeration< ? > groupSearchResults = ctx.search(ldapConfig.getBaseDn(), "(member=" + dn + ")", getSimpleSearchControls());
    while (groupSearchResults.hasMore()) {
      SearchResult groupSearchResult = (SearchResult) groupSearchResults.next();
      String groupName = groupSearchResult.getAttributes().get(ldapConfig.getGroupNameAttribute()).get().toString();
      groupIds.add(new TaskListGroup(groupName, groupName));
    }

    return groupIds;
  }
}
