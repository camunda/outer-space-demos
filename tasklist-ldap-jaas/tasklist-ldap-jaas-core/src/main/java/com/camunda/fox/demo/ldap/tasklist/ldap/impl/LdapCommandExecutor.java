package com.camunda.fox.demo.ldap.tasklist.ldap.impl;

import java.util.Hashtable;

import javax.inject.Inject;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import com.camunda.fox.demo.ldap.tasklist.ldap.LdapConfiguration;


public class LdapCommandExecutor {
  
  @Inject
  private LdapConfiguration ldapConfig;

  /**
   * execute an {@link LdapCommand} for the technical user (e.g. to query
   * colleagues or groups)
   */
  public <T> T execute(LdapCommand<T> cmd) {
    return execute(cmd, ldapConfig.getLdapUser(), ldapConfig.getLdapPassword());
  }

  /**
   * execute an {@link LdapCommand} for the given user
   */
  public <T> T execute(LdapCommand<T> cmd, String user, String password) {
    Hashtable<String, String> env = new Hashtable<String, String>();
    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    env.put(Context.SECURITY_AUTHENTICATION, "simple");
    env.put(Context.PROVIDER_URL, ldapConfig.getLdapServer());
    if (ldapConfig.isUseSSL()) {
      // Remove if you have the SSL Key correctly in your keystore
      SslUnsecureTrustManagerHelper.accepptAllSSLCertificates();
      env.put(Context.SECURITY_PROTOCOL, "ssl");
    }
    env.put(Context.SECURITY_PRINCIPAL, user);
    env.put(Context.SECURITY_CREDENTIALS, password);

    DirContext ctx = null;
    try {
      ctx = new InitialDirContext(env);
      return cmd.execute(ctx, ldapConfig);
    } catch (AuthenticationException ex) {
      throw new IllegalArgumentException("Invalid Credentials", ex);
    } catch (Exception ex) {
      throw new IllegalArgumentException("Error while talking to LDAP server", ex);

    } finally {
      if (ctx != null) {
        try {
          ctx.close();
        } catch (Exception e) {
          // Ignore
        }
      }
    }
  }
}
