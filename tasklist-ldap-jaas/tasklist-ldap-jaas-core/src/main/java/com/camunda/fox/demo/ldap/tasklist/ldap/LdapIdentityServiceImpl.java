package com.camunda.fox.demo.ldap.tasklist.ldap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.activiti.cdi.impl.util.ProgrammaticBeanLookup;

import com.camunda.fox.demo.ldap.tasklist.ldap.impl.GetColleaguesForUserCommand;
import com.camunda.fox.demo.ldap.tasklist.ldap.impl.GetDnForUserCommand;
import com.camunda.fox.demo.ldap.tasklist.ldap.impl.GetGroupsForUserCommand;
import com.camunda.fox.demo.ldap.tasklist.ldap.impl.LdapCommandExecutor;
import com.camunda.fox.demo.ldap.tasklist.ldap.impl.NoopCommand;
import com.camunda.fox.tasklist.api.TaskListGroup;
import com.camunda.fox.tasklist.api.TasklistIdentityService;
import com.camunda.fox.tasklist.api.TasklistUser;

public class LdapIdentityServiceImpl implements TasklistIdentityService, Serializable {

  private static final long serialVersionUID = 1L;
  
  @Override
  public void authenticateUser(final String userId, String password) {
	final LdapCommandExecutor ldap = ProgrammaticBeanLookup.lookup(LdapCommandExecutor.class);
	
    String dn = ldap.execute(new GetDnForUserCommand(userId));

    // now try to log on with this user and the given password
    ldap.execute(new NoopCommand(), dn, password);
  }

  @Override
  public List<TaskListGroup> getGroupsByUserId(String userId) {
	final LdapCommandExecutor ldap = ProgrammaticBeanLookup.lookup(LdapCommandExecutor.class);
    return ldap.execute(new GetGroupsForUserCommand(userId));
  }

  @Override
  public List<TasklistUser> getColleaguesByUserId(final String userId) {
	final LdapCommandExecutor ldap = ProgrammaticBeanLookup.lookup(LdapCommandExecutor.class);
    List<TasklistUser> result = new ArrayList<TasklistUser>();
    result.addAll(ldap.execute(new GetColleaguesForUserCommand(userId)));

    // Maybe add some hard coded colleagues for testing purposes
    // result.add(new User("kermit", "Kermit", "The Frog"));
    // result.add(new User("gonzo", "Gonzo", "The Great"));
    // result.add(new User("fozzie", "Fozzie", "Bear"));

    return result;
  }

}
