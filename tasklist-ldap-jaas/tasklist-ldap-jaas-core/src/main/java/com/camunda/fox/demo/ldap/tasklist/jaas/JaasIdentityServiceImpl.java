package com.camunda.fox.demo.ldap.tasklist.jaas;

import java.io.Serializable;
import java.security.Principal;
import java.util.List;

import org.activiti.cdi.impl.util.ProgrammaticBeanLookup;

import com.camunda.fox.demo.ldap.tasklist.ldap.impl.GetColleaguesForUserCommand;
import com.camunda.fox.demo.ldap.tasklist.ldap.impl.GetGroupsForUserCommand;
import com.camunda.fox.demo.ldap.tasklist.ldap.impl.LdapCommandExecutor;
import com.camunda.fox.tasklist.api.TaskListGroup;
import com.camunda.fox.tasklist.api.TasklistIdentityService;
import com.camunda.fox.tasklist.api.TasklistUser;

public class JaasIdentityServiceImpl implements TasklistIdentityService, Serializable {

  private static final long serialVersionUID = 1L;

  public void authenticateUser(final String userId, String password) {
    // JAAS does the authentication, we do not have to check anything here
  }
  
  public List<TaskListGroup> getGroupsByUserId(String userId) {
    // we still have to get these information from somewhere, maybe still
    // accessing LDAP directly...
	Principal principal = ProgrammaticBeanLookup.lookup(Principal.class);
	LdapCommandExecutor ldap = ProgrammaticBeanLookup.lookup(LdapCommandExecutor.class);
    return ldap.execute(new GetGroupsForUserCommand(principal.getName()));
  }

  @Override
  public List<TasklistUser> getColleaguesByUserId(String arg0) {
	Principal principal = ProgrammaticBeanLookup.lookup(Principal.class);
	LdapCommandExecutor ldap = ProgrammaticBeanLookup.lookup(LdapCommandExecutor.class);
    return ldap.execute(new GetColleaguesForUserCommand(principal.getName()));
}
}
