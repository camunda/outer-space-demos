package com.camunda.fox.demo.ldap.tasklist.ldap.impl;

import javax.naming.directory.DirContext;

import com.camunda.fox.demo.ldap.tasklist.ldap.LdapConfiguration;

/**
 * Noop Command, handy to check if log in was successful
 */
public class NoopCommand extends LdapCommand<Void> {

  @Override
  public Void execute(DirContext ctx, LdapConfiguration ldapConfig) throws Exception {
    // do nothing, if we made it to here, the login was successful :-)
    return null;
  }
}
