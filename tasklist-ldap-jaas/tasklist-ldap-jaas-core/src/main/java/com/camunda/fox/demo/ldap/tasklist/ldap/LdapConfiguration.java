package com.camunda.fox.demo.ldap.tasklist.ldap;

/*
 * Contains all configuration parameters for LDAP
 */
public class LdapConfiguration {

  private static final String BASE_DN = "o=camunda,c=com";
  private static final String USER_ID_ATTRIBUTE = "uid";
  private static final String GROUP_NAME_ATTRIBUTE = "cn";

  // Properties for local ApacheDS
  private static final boolean LDAP_USE_SSL = false;
  private static final String LDAP_SERVER = "ldap://localhost:389/";
  private static final String LDAP_PASSWORD = "secret";
  private static final String LDAP_USER = "uid=admin,ou=system";

  public String getBaseDn() {
    return BASE_DN;
  }

  public String getUserIdAttribute() {
    return USER_ID_ATTRIBUTE;
  }

  public String getGroupNameAttribute() {
    return GROUP_NAME_ATTRIBUTE;
  }

  public boolean isUseSSL() {
    return LDAP_USE_SSL;
  }

  public String getLdapServer() {
    return LDAP_SERVER;
  }

  public String getLdapPassword() {
    return LDAP_PASSWORD;
  }

  public String getLdapUser() {
    return LDAP_USER;
  }

}
