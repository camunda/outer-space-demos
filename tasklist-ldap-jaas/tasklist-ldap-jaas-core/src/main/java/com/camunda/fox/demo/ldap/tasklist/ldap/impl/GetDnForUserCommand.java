package com.camunda.fox.demo.ldap.tasklist.ldap.impl;

import javax.naming.NamingEnumeration;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchResult;

import com.camunda.fox.demo.ldap.tasklist.ldap.LdapConfiguration;

public class GetDnForUserCommand extends LdapCommand<String> {

  private String userId;

  public GetDnForUserCommand(String userId) {
    this.userId = userId;
  }

  @Override
  public String execute(DirContext ctx, LdapConfiguration ldapConfig) throws Exception {
    NamingEnumeration< ? > userIdEnum = ctx.search(ldapConfig.getBaseDn(), "(" + ldapConfig.getUserIdAttribute() + "="+userId+")", getSimpleSearchControls());
    if (!userIdEnum.hasMore()) {
      // dont' do this in production, it is best practice to not let the user
      // know if user or password is the problem
      throw new IllegalArgumentException("User '" + userId + "' not found");
    }
    SearchResult userSearchResult = (SearchResult) userIdEnum.next();
    return userSearchResult.getNameInNamespace();
  }
}
