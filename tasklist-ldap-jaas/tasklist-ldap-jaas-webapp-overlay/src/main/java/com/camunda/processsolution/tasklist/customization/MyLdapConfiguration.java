package com.camunda.processsolution.tasklist.customization;

import javax.enterprise.inject.Specializes;

import com.camunda.fox.demo.ldap.tasklist.ldap.LdapConfiguration;

@Specializes
public class MyLdapConfiguration extends LdapConfiguration {

    // TODO: Override to your LDAP configuration!
}
