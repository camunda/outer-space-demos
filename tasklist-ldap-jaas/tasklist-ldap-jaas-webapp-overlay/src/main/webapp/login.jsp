<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>camunda fox tasklist</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link type="text/css" rel="stylesheet" href="/tasklist/javax.faces.resource/css/bootstrap.css.jsf" />
  <style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>
  <link type="text/css" rel="stylesheet" href="/tasklist/javax.faces.resource/css/responsive.css.jsf" />
</head>

<body>

  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container-fluid" style="padding: 0;">
        <a class="brand" href="#" style="margin:0 0 0 17px; padding:0;"><img
          src="/tasklist/resources/img/FoxTasklist_white.png"
          alt="camunda fox - task list" /></a>
      </div>
    </div>
  </div>

  <hr />
  <div class="container-fluid">
    <div id="content">
      <div class="row">
        <div class="well span4 offset4">      
       	   <h1>Login</h1>
		   <form method=POST action="j_security_check">
		      <label><strong>Username:</strong>
		        <input type="text" size="20" name="j_username" />
		      </label>
		      <label><strong>Password:</strong>
		        <input type="password" size="20" name="j_password" />
		      </label>
		      <input type="submit" name="login" value="Login" class="btn btn-primary" />
		   </form>
		 </div>
      </div>
    </div>


    <hr />

    <footer>
      <p>
        powered by <a href="http://www.camunda.com/fox/">camunda fox</a>
      </p>
    </footer>

  </div>
</body>
</html>
