package com.camunda.fox.demo.outerspace.threadmanagement;

import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;

public class ProcessTestCase extends ActivitiTestCase {
	  
  @Deployment(resources = "thread-management-demo.bpmn")
  public void testStartManagent() {
//    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("thread-management-demo");
    
    ProcessDefinition pd = processEngine.getRepositoryService().createProcessDefinitionQuery().processDefinitionKey("feature-documentation").processDefinitionVersion(1).singleResult();
    processEngine.getRuntimeService().startProcessInstanceById(pd.getId());
  }
 
//  @Deployment(resources = "thread-management-demo.bpmn")
//  public void testSomething() {
//    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("thread-management-demo");
//  }
//  @Deployment(resources = "thread-management-demo.bpmn")
//  public void testSomething() {
//    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("thread-management-demo");
//  }
}