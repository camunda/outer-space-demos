package com.camunda.fox.demo.outerspace.threadmanagement;

import javax.inject.Inject;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.runtime.ProcessInstance;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;


/**
 * Test case running on the fox platform via Arquillian.
 * Requires a running fox-platform.
 * 
 * @author nico.rehwaldt
 */
@RunWith(Arquillian.class)
public class ExplicitEscalationPlatformTest {

  @Inject
  private ProcessEngine engine;
  
  @Deployment
  public static Archive<?> createDeployment() {
    
    MavenDependencyResolver resolver = DependencyResolvers
      .use(MavenDependencyResolver.class)
        .loadMetadataFromPom("pom.xml");

    Archive<?> archive = ShrinkWrap
      .create(WebArchive.class, "docs.war")
        // prepare as process application archive for fox platform
        .addAsLibraries(resolver.artifact("com.camunda.fox.platform:fox-platform-client").resolveAsFiles())
        .addAsResource("META-INF/processes.xml")
        .addAsWebResource(EmptyAsset.INSTANCE, "WEB-INF/beans.xml")
        // add your own classes (could be done one by one as well)
//        .addPackages(false, "com.camunda.fox.quickstart.escalation.explicit")
        // add process definition
        .addAsResource("processversioning/long-running-process-old.bpmn")
        .addAsResource("processversioning/long-running-process-new.bpmn", "processversioning/long-running-process-new");
//        .addAsResource("threadmanagement/thread-management-demo.bpmn");
    
    return archive;
  }
  
//  @Test
//  public void startThreadManagementDemo() throws Exception {
//    ProcessInstance processInstance = engine.getRuntimeService().startProcessInstanceByKey("thread-management-demo");
//  }
  
  @Test
  public void startLongRunningProcesDemo() throws Exception {
    engine.getRuntimeService().startProcessInstanceByKey("feature-documentation");
    engine.getRuntimeService().startProcessInstanceByKey("feature-documentation");
    
    engine.getRepositoryService().createDeployment().addInputStream("processversioning/long-running-process-new.bpmn", this.getClass().getResourceAsStream("/processversioning/long-running-process-new")).deploy();
    engine.getRuntimeService().startProcessInstanceByKey("feature-documentation");
  }
}
