import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.ProcessEngineImpl;
import org.activiti.engine.impl.cmd.SetProcessDefinitionVersionCmd;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Job;

import com.camunda.fox.client.impl.ProcessArchiveSupport;
import com.camunda.fox.platform.api.ProcessEngineService;


/**
 * Class holding code snippets used in production. It doesn't do anything useful but it validates that all 
 * code examples still compile.
 */
public class CodeSnippets {

  @Inject
  private ProcessEngine processEngine;
  
  @EJB(lookup = ProcessArchiveSupport.PROCESS_ENGINE_SERVICE_NAME)
  private ProcessEngineService processEngineService;
  
  public void startSpecificVersion() {
    ProcessDefinition pd = processEngine.getRepositoryService().createProcessDefinitionQuery().processDefinitionKey("feature-documentation").processDefinitionVersion(1).singleResult();
    processEngine.getRuntimeService().startProcessInstanceById(pd.getId());
  }
  
  public void migrateVersion() {
    String processInstanceId = "d4c353cf-afce-11e1-8a89-f0def123c6bd";
    int newVersion = 2;
    SetProcessDefinitionVersionCmd setProcessDefinitionVersionCmd = new SetProcessDefinitionVersionCmd(processInstanceId, newVersion);
    ((ProcessEngineImpl) processEngineService.getDefaultProcessEngine()).getProcessEngineConfiguration().getCommandExecutorTxRequired().execute(setProcessDefinitionVersionCmd);
  }
  
  @Inject
  private RepositoryService repositoryService;
  
  public void deploy() throws FileNotFoundException { 
    InputStream is = new FileInputStream("C:/myprocess.bpmn");    
    repositoryService.createDeployment()
        .addInputStream("myprocess.bpmn", is)
        .deploy();
  }

  public void readReassource() { 
    ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionKey("myprocess").latestVersion().singleResult();
    repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), "some-resource.txt");
  }
  
  public void suspend() {
//    processEngine.getRepositoryService().de
  }
  
  public void tenant() {    
    String TENANT_VARIABLE_NAME = "TENANT_ID";
    
    HashMap<String, Object> variables = new HashMap<String, Object>();
    variables.put(TENANT_VARIABLE_NAME, "camunda");
    processEngine.getRuntimeService().startProcessInstanceByKey("some-process", variables);
    
    // get tasks only for this tenant
    processEngine.getTaskService().createTaskQuery().processVariableValueEquals(TENANT_VARIABLE_NAME, "camunda").list();

    // query instances only for this tenant (e.g. for monitoring):
    processEngine.getRuntimeService().createProcessInstanceQuery().variableValueEquals(TENANT_VARIABLE_NAME, "camunda");
  }
  
  public void failedJobs() {    
    List<Job> failedJobs = processEngine.getManagementService().createJobQuery().withException().list();
    for (Job failedJob : failedJobs) {
      processEngine.getManagementService().setJobRetries(failedJob.getId(), 1);
    }
  }
  
//  @EJB(lookup = ProcessArchiveSupport.PROCESS_ENGINE_SERVICE_NAME)
//  private ProcessEngineService processEngineService;

  public void callOtherEngine() {
       processEngineService.getProcessEngine("process-engine-a").getRuntimeService().startProcessInstanceByKey("child");
  }
  
  public String getPageflowStartForm() {
    String formKey = processEngine.getFormService().getTaskFormData("taskId").getFormKey();
//    String url = getStartFormUrlForPageflow(formKey);
    String url = "";
    return url;
  }
}
