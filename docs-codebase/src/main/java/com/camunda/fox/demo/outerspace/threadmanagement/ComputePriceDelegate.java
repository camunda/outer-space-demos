package com.camunda.fox.demo.outerspace.threadmanagement;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;


public class ComputePriceDelegate implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    new PricingService().computePrice("");
  }

}
