
$(document).ready(function(){

	cm = new ControlManager();	
	
 });

function controlConnect() {
	controlUnsubscribe();
    controlSubscribe();
}

// jquery.atmosphere.response
function callback(response) {
	
	var controlManager; 

	if (response.transport != 'polling' && response.state == 'messageReceived') {
		$.atmosphere.log('info', ["response.responseBody: " + response.responseBody]);
		if (response.status == 200) {
			var dataString = response.responseBody;
			var data = jQuery.parseJSON(dataString);
			if (dataString.length > 0) {
				$.each(this.callback.controlManager.diagramLayout['nodes'], function(index, value){
					if(data[value['id']]>0) {
						
						var styleClass = value['id'].indexOf('Service') != -1 ? "label-warning" : "label-info";
						$("#pdnode-" + value['id']).remove();
						
						$("#processDiagram").append(
							'<div id=\"pdnode-' + value['id'] +'\"' +
							'style=\"position: absolute;' +
							'left: '+(value['x']+(value['width']/2)-5) + 'px; ' + 
							'top: '+ (value['y']+value['height']-5) + 'px; '+
							'width: '+ (value['width']-2) + 'px; '+
							'height: '+ (value['height']-2) + 'px; '+
							'\"> <span class="label '+styleClass+'\" style=\"font-size:20pt\">'+ data[value['id']] +'</span> </div>');
					} else {
						$("#pdnode-" + value['id']).remove();
					}
					
											
				});
			}
		}
	}
}

function controlSubscribe() {
	if (!this.callbackAdded) {
		
		var location = document.location.toString();
		location = location.substring(0,location.lastIndexOf('/')) + '/monitor/control';		
		this.connectedEndpoint = $.atmosphere.subscribe(location,
			!callbackAdded ? this.callback : null,
			$.atmosphere.request = { 
				transport: 'websocket' 
			}
		);
		
		this.callbackAdded = true;
	}	
}

function controlUnsubscribe(){
	this.callbackAdded = false;
	$.atmosphere.unsubscribe();
}

function ControlManager() {
	
	var self =this;
	var diagramLayout;
	
	var location = document.location.toString();
	location = location.substring(0,location.lastIndexOf('/')) + '/rest/processDiagramLayout/newTpdInsurance';	
	
	var request = $.ajax({
		url: location,
		type: 'GET',
		contentType:"application/json; charset=utf-8",
	}).success(function(data) { 
		self.diagramLayout = data;
	});
	
	this.suscribe = controlSubscribe;
	this.unsubscribe = controlUnsubscribe;
	this.connect = controlConnect;
	this.callback = callback;
	this.callback.controlManager = this;
	this.connect();
	this.actions = cm_actions;
}

function post() {
	
	var request = $.ajax({
		url: 'rest/monitor/toggle',
		type: 'POST',
		data: '<doc>foo</doc>',
		contentType:"application/xml; charset=utf-8",
	});
				
	request.fail(function(jqXHR, textStatus) {
  		alert( "Request failed: " + textStatus );
	});
}