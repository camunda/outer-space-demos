/**
 * The About view
 */
define([
    'backbone',
    'utilities',
    'text!../../../../templates/desktop/products.html'
], function (Backbone, utilities, productsTemplate) {

    var ProductsView = Backbone.View.extend({
        render:function () {
            utilities.applyTemplate($(this.el),productsTemplate,{model:this.model});
            return this;
        }
    });

    return ProductsView;
});