/**
 * The About view
 */
define([
    'backbone',
    'utilities',
    'text!../../../../templates/mobile/products.html'
], function (Backbone, utilities, productsTemplate) {

    var ProductsView = Backbone.View.extend({
        render:function () {
            utilities.applyTemplate($(this.el),productsTemplate,{model:this.model});
            $(this.el).trigger('pagecreate');
            return this;
        }
    });

    return ProductsView;
});