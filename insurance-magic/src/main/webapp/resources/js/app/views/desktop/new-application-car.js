/**
 * 
 */
define([
    'backbone',
    'utilities',
    'text!../../../../templates/desktop/new-application-car.html',
    'text!../../../../templates/desktop/confirmation.html'
], function (Backbone, utilities, template, confirmationTemplate) {

    var NewApplicationCar = Backbone.View.extend({
    	events:{
            "keyup #inputEmail":"updateEmail",
            "change #inputEmail":"updateEmail",
            
            "keyup #inputName":"updateName",
            "change #inputName":"updateName",
            
            "keyup #inputBrand":"updateBrand",
            "change #inputBrand":"updateBrand",
            
            "keyup #inputYearOfManufacture":"updateYearOfManufacture",
            "change #inputYearOfManufacture":"updateYearOfManufacture",
            
            "click input[name='submit']":"save"
        },
        render:function () {
        	this.model.email = "";
        	this.model.name = "";
        	this.model.brand = "";
        	this.model.yearOfManufacture = "";
            utilities.applyTemplate($(this.el),template,{model:this.model});
            return this;
        },
        
        save:function (event) {
        	var self = this;
            var newCarInsuranceRequest = {
              name : self.model.name,
              email : self.model.email,
              brand : self.model.brand,
              yearOfManufacture : self.model.yearOfManufacture,
            };           
            $("input[name='submit']").attr("disabled", true)
            $.ajax({url:"rest/application/car/",
                data:JSON.stringify(newCarInsuranceRequest),
                type:"POST",
                dataType:"json",
                contentType:"application/json",
                success:function (application) {
                    this.model = {}
                    utilities.applyTemplate($("#content"), confirmationTemplate, {model:application});
                }}).error(function (error) {
                    if (error.status == 400 || error.status == 409) {
                    $("#request-summary").append('<div class="alert alert-error"><a class="close" data-dismiss="alert">�</a><strong>Error! </strong>An error has occured: '+error.responseText+'</div>')
                    $("input[name='submit']").removeAttr("disabled");
                    }
                })

        },
        
        updateEmail:function (event) {
            if ($(event.currentTarget).is(':valid')) {
                this.model.email = event.currentTarget.value;
                $("#inputEmail-error").empty();
            } else {
                $("#inputEmail-error").empty().append("Bitte geben Sie eine valide Email adresse ein.");
                delete this.model.email;
            }
            this.setCreateApplicationStatus();
        },
        updateName:function (event) {
            if (event.currentTarget.value.length<3) {
            	$("#inputName-error").empty().append("Muss mehr als 3 Zeichen lang sein.");
            	delete this.model.name;
            } else if(event.currentTarget.value.length>25) {
            	$("#inputName-error").empty().append("Muss weniger als 25 Zeichen lang sein.");
                 delete this.model.name;
            } else {
            	this.model.name = event.currentTarget.value;
                $("#inputName-error").empty();
            }
            this.setCreateApplicationStatus();
        },
        updateBrand:function (event) {          
        	this.model.brand = event.currentTarget.value;        
            this.setCreateApplicationStatus();
        },        
        updateYearOfManufacture:function (event) {            
            this.model.yearOfManufacture = event.currentTarget.value;     
            this.setCreateApplicationStatus();
        },
        setCreateApplicationStatus:function() {        	
        	if(!(typeof this.model.name === 'undefined')
        	   && !(typeof this.model.email === 'undefined')
        	   && !(typeof this.model.brand === 'undefined')
        	   && !(typeof this.model.yearOfManufacture === 'undefined')) {
        	   $('input[name="submit"]').removeAttr('disabled');
        	} else {
        		$('input[name="submit"]').attr('disabled', true);
        	}
        },
    });

    return NewApplicationCar;
});