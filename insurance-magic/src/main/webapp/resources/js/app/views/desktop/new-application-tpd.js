/**
 * 
 */
define([
    'backbone',
    'utilities',
    'text!../../../../templates/desktop/new-application-tpd.html',
    'text!../../../../templates/desktop/confirmation.html'
], function (Backbone, utilities, template, confirmationTemplate) {

    var NewApplicationTpdView = Backbone.View.extend({
    	events:{
            "keyup #inputEmail":"updateEmail",
            "change #inputEmail":"updateEmail",
            
            "keyup #inputName":"updateName",
            "change #inputName":"updateName",
            
            "keyup #inputJob":"updateJob",
            "change #inputJob":"updateJob",
            
            "keyup #inutDateOfBirth":"updateDateOfBirth",
            "change #inutDateOfBirth":"updateDateOfBirth",
            
            "keyup #inputMonthlyPayment":"updateMonthlyPayment",
            "change #inputMonthlyPayment":"updateMonthlyPayment",
            
            "keyup #inputResult":"updateResult",
            "change #inputResult":"updateResult",
            
            "click input[name='submit']":"save"
        },
        render:function () {
        	this.model.name = "";
        	this.model.email = "";
        	this.model.job = "";
        	this.model.dateOfBirth = "";
        	this.model.monthlyPayment = "";
        	this.model.result = "";
            utilities.applyTemplate($(this.el),template,{model:this.model});
            return this;
        },
        
        save:function (event) {
        	var self = this;
            var newInsuranceRequest = {
              name : self.model.name,
              email : self.model.email,
              job : self.model.job,
              dateOfBirth : self.model.dateOfBirth,
              monthlyPayment : self.model.monthlyPayment,
              result : self.model.result            
            };           
            $("input[name='submit']").attr("disabled", true)
            $.ajax({url:"rest/application/tpd/",
                data:JSON.stringify(newInsuranceRequest),
                type:"POST",
                dataType:"json",
                contentType:"application/json",
                success:function (application) {
                    this.model = {}
                    utilities.applyTemplate($("#content"), confirmationTemplate, {model:application});
                }}).error(function (error) {
                    if (error.status == 400 || error.status == 409) {
                    $("#request-summary").append('<div class="alert alert-error"><a class="close" data-dismiss="alert">�</a><strong>Error! </strong>An error has occured: '+error.responseText+'</div>')
                    $("input[name='submit']").removeAttr("disabled");
                    }
                })

        },
        
        updateEmail:function (event) {
            if ($(event.currentTarget).is(':valid')) {
                this.model.email = event.currentTarget.value;
                $("#inputEmail-error").empty();
            } else {
                $("#inputEmail-error").empty().append("Bitte geben Sie eine valide Email adresse ein.");
                delete this.model.email;
            }
            this.setCreateApplicationStatus();
        },
        updateName:function (event) {
            if (event.currentTarget.value.length<3) {
            	$("#inputName-error").empty().append("Muss mehr als 3 Zeichen lang sein.");
            	delete this.model.name;
            } else if(event.currentTarget.value.length>25) {
            	$("#inputName-error").empty().append("Muss weniger als 25 Zeichen lang sein.");
                 delete this.model.name;
            } else {
            	this.model.name = event.currentTarget.value;
                $("#inputName-error").empty();
            }
            this.setCreateApplicationStatus();
        },
        updateJob:function (event) {          
        	this.model.job = event.currentTarget.value;        
            this.setCreateApplicationStatus();
        },        
        updateDateOfBirth:function (event) {            
            this.model.dateOfBirth = event.currentTarget.value;     
            this.setCreateApplicationStatus();
        },
        updateMonthlyPayment:function (event) {            
            this.model.monthlyPayment = event.currentTarget.value;     
            this.setCreateApplicationStatus();
        },
        updateResult:function (event) {            
            this.model.result = event.currentTarget.value;     
            this.setCreateApplicationStatus();
        },
        setCreateApplicationStatus:function() {        	
        	if(!(typeof this.model.name === 'undefined')
        	   && !(typeof this.model.email === 'undefined')
        	   && !(typeof this.model.job === 'undefined')
        	   && !(typeof this.model.monthlyPayment === 'undefined')
        	   && !(typeof this.model.result === 'undefined')
        	   && !(typeof this.model.dateOfBirth === 'undefined')) {
        	   $('input[name="submit"]').removeAttr('disabled');
        	} else {
        		$('input[name="submit"]').attr('disabled', true);
        	}
        }
    });

    return NewApplicationTpdView;
});