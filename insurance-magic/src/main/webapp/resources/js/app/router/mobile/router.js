/**
 * A module for the router of the desktop application.
 *
 */
define("router",[
    'jquery',
    'jquerymobile',
    'underscore',
    'backbone',
    'utilities',
    'app/models/product',
    'app/collections/products',
    'app/views/mobile/products',
//    'app/views/mobile/new-application-car',
//    'app/views/mobile/new-application-tpd',
    'text!../templates/mobile/home-view.html'
],function ($,
            jqm,
            _,
            Backbone,
            utilities,
            Product,
            Products,
            HomeView,
            ProductsView,
//            NewApplicationCarView,
//            NewApplicationTpdView,
            HomeViewTemplate) {

    // prior to creating an starting the router, we disable jQuery Mobile's own routing mechanism
    $.mobile.hashListeningEnabled = false;
    $.mobile.linkBindingEnabled = false;
    $.mobile.pushStateEnabled = false;

    /**
     * The Router class contains all the routes within the application - i.e. URLs and the actions
     * that will be taken as a result.
     *
     * @type {Router}
     */
    var Router = Backbone.Router.extend({
        routes:{
        	"":"home",
            "about":"home",
            "products":"products",
//            "products/car":"car",
//            "products/tpd":"tpd"
        },       
        home:function () {
            utilities.applyTemplate($("#container"), HomeViewTemplate);
            try {
                $("#container").trigger('pagecreate');
            } catch (e) {
                // workaround for a spurious error thrown when creating the page initially
            }
        },
        products:function () {
            var products = new Products();
            var productsView = new ProductsView({model:products, el:$("#content")});
            products.bind("reset",
                function () {
                    utilities.viewManager.showView(productsView);
            }).fetch();
        },          
//        car:function() {
//        	var newApplicationCarView = new NewApplicationCarView({model:{},el:$("#content")});
//        	utilities.viewManager.showView(newApplicationCarView);
//        },
//        tpd:function() {
//        	var newApplicationTpdView = new NewApplicationTpdView({model:{},el:$("#content")});
//        	utilities.viewManager.showView(newApplicationTpdView);
//        }        
    });

    // Create a router instance
    var router = new Router();

    // Begin routing
    Backbone.history.start();

    return router;
});