/**
 * A module for the router of the desktop application
 */
define("router", [
    'jquery',
    'underscore',
    'backbone',
    'utilities',
    'app/models/product',
    'app/collections/products',
    'app/views/desktop/home',
    'app/views/desktop/products',
    'app/views/desktop/new-application-car',
    'app/views/desktop/new-application-tpd',
],function ($,
            _,
            Backbone,
            utilities,
            Product,
            Products,
            HomeView,
            ProductsView,
            NewApplicationCarView,
            NewApplicationTpdView
            ) {

    /**
     * The Router class contains all the routes within the application - 
     * i.e. URLs and the actions that will be taken as a result.
     *
     * @type {Router}
     */

    var Router = Backbone.Router.extend({
        routes:{
            "":"home",
            "about":"home",
            "products":"products",
            "products/car":"car",
            "products/tpd":"tpd"
        },
        products:function () {
            var products = new Products();
            var productsView = new ProductsView({model:products, el:$("#content")});
            products.bind("reset",
                function () {
                    utilities.viewManager.showView(productsView);
            }).fetch();
        },      
        home:function () {
            utilities.viewManager.showView(new HomeView({el:$("#content")}));
        },
        car:function() {
        	var newApplicationCarView = new NewApplicationCarView({model:{},el:$("#content")});
        	utilities.viewManager.showView(newApplicationCarView);
        },
        tpd:function() {
        	var newApplicationTpdView = new NewApplicationTpdView({model:{},el:$("#content")});
        	utilities.viewManager.showView(newApplicationTpdView);
        }
    
    });

    // Create a router instance
    var router = new Router();

    //Begin routing
    Backbone.history.start();

    return router;
});