/**
 * The module for a collection of Products
 */
define([
    // Backbone and the collection element type are dependencies
    'backbone',
    'app/models/product'
], function (Backbone, Product) {

    // Here we define the Applications collection
    // We will use it for CRUD operations on Products

    var Products = Backbone.Collection.extend({
        url:'rest/products',
        model: Product,
        id:'id'
    });

    return Products;
});