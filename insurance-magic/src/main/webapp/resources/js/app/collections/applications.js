/**
 * The module for a collection of Applications
 */
define([
    // Backbone and the collection element type are dependencies
    'backbone',
    'app/models/application'
], function (Backbone, Application) {

    // Here we define the Applications collection
    // We will use it for CRUD operations on Applicatuions

    var Applications = Backbone.Collection.extend({
        url:'rest/applications',
        model: Application,
        id:'id'
    });

    return Applications;
});