/**
 * Module for the Insurance Product model
 */
define([
    // Backbone is a dependency
    'backbone'
], function (Backbone) {

    /**
     * The Product model class definition
     * Used for CRUD operations against individual products
     */
    var Product = Backbone.Model.extend({
        urlRoot:'rest/products'
    });

    return Product;

});