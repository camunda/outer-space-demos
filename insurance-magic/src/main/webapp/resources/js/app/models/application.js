/**
 * Module for the Insurance Application model
 */
define([
    // Backbone is a dependency
    'backbone'
], function (Backbone) {

    /**
     * The Booking model class definition
     * Used for CRUD operations against individual applications
     */
    var Application = Backbone.Model.extend({
        urlRoot:'rest/applications'
    });

    return Application;

});