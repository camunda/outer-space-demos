package com.camunda.fox.showcase.insurance.view.rest;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

import com.camunda.fox.showcase.insurance.persistence.entity.Product;

@Path("/products")
@Stateless
public class ProductResource extends BaseEntityService<Product> {
  
  public ProductResource() {
    super(Product.class);
  }

}
