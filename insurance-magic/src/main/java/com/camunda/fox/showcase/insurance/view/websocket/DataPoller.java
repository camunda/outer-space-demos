package com.camunda.fox.showcase.insurance.view.websocket;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.DiagramLayout;
import org.activiti.engine.repository.DiagramNode;
import org.activiti.engine.repository.ProcessDefinition;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;

import com.camunda.fox.showcase.insurance.persistence.entity.TpdInsuranceAppliation;

@Singleton
//make sure this is started after the processes are deployed.
@DependsOn("ProcessArchiveSupport") 
public class DataPoller {

  @Inject
  private HistoryService historyService;
  
  @Inject
  private RepositoryService repositoryService;
  
  @Inject
  private EntityManager entityManager;
  
  @Inject RuntimeService runtimeService;

  private DiagramLayout processDiagramLayout;
  private String processDefinitionId;
  private Map<String, Long> countsBefore;
  
  private Map<String, Integer> allCustomers = new HashMap<String, Integer>();
  private List<String> topCustomers = new ArrayList<String>();
  
  @PostConstruct
  protected void initialize() {
    
    ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
       .processDefinitionKey("newTpdInsurance")
       .latestVersion()
       .singleResult();
    processDefinitionId = processDefinition.getId();
          
    processDiagramLayout = repositoryService.getProcessDiagramLayout(processDefinitionId);
    
  }

  @Schedule(second = "*/5", minute = "*/1", hour = "*", persistent = false)
  public void pollProcessInstanceState() {
    Map<String, Long> activityInstanceCounts = new HashMap<String, Long>();
    
    StringWriter jsonWriter = new StringWriter();
    jsonWriter.write("{");
    
    for (int i = 0; i < processDiagramLayout.getNodes().size(); i++) {
      DiagramNode node = processDiagramLayout.getNodes().get(i);
      // TODO: get all the counts in a single DB query!!
      long count = 0;
      
      if(node.getId().contains("End")) {
        count = historyService.createHistoricActivityInstanceQuery()
          .activityId(node.getId())
          .processDefinitionId(processDefinitionId)
          .finished()
          .count();
      } else {
        count = runtimeService.createExecutionQuery()
          .activityId(node.getId())
          .processDefinitionId(processDefinitionId)
          .count();
      }
      
      jsonWriter.write("\""+node.getId()+"\"");
      jsonWriter.write(" : ");
      jsonWriter.write(String.valueOf(count));     
      if(i != (processDiagramLayout.getNodes().size()-1)) {
        jsonWriter.write(", ");
      }
    }
    
    jsonWriter.write("}");
    
    String result = jsonWriter.toString();

//    if(!activityInstanceCounts.equals(countsBefore)){
      Broadcaster broadcaster = BroadcasterFactory.getDefault().lookup("control", true);
      if (broadcaster != null) {
        broadcaster.broadcast(result);
      }
      countsBefore = activityInstanceCounts;      
      
//    }

  }

}
