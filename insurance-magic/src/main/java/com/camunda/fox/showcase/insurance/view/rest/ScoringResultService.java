package com.camunda.fox.showcase.insurance.view.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;

@Path("scoring")
public class ScoringResultService {

  @Inject
  private RuntimeService runtimeService;
  
  @GET
  @Path("{antragsId}")
  public void scoringResult(@PathParam("antragsId") Long antragsId) {
    
    ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
      .variableValueEquals("applicationId", antragsId)
      .active()
      .singleResult();
    
    Execution execution = runtimeService.createExecutionQuery()
      .messageEventSubscriptionName("scoringResult")
      .processInstanceId(processInstance.getId())
      .singleResult();
    
    runtimeService.messageEventReceived("scoringResult", execution.getId());
  }
  
}
