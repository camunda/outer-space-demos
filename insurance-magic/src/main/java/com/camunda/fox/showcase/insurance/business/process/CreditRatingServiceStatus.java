package com.camunda.fox.showcase.insurance.business.process;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CreditRatingServiceStatus {

  private boolean isAvailable = true;

  public void setAvailable(boolean isAvailable) {
    this.isAvailable = isAvailable;
  }

  public boolean isAvailable() {
    return isAvailable;
  }
}
