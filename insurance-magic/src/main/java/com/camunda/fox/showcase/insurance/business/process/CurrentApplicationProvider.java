package com.camunda.fox.showcase.insurance.business.process;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.activiti.cdi.BusinessProcess;

import com.camunda.fox.showcase.insurance.persistence.entity.AbstractInsuranceApplication;
import com.camunda.fox.showcase.insurance.persistence.entity.CarInsuranceAppliation;
import com.camunda.fox.showcase.insurance.persistence.entity.TpdInsuranceAppliation;


public class CurrentApplicationProvider {
  
  @Inject
  private EntityManager entityManager;
  
  @Inject 
  private BusinessProcess businessProcess;
  
  @Produces
  @Current
  @Named("currentApplication")
  public AbstractInsuranceApplication insuranceApplication() {
    
    String type = businessProcess.getVariable("applicationType");
    Long applicationId = businessProcess.getVariable("applicationId");
    
    if(type.equals("tpd")) {
      return entityManager.find(TpdInsuranceAppliation.class, applicationId);
    }else {
      return entityManager.find(CarInsuranceAppliation.class, applicationId);
    }
    
  }

}
