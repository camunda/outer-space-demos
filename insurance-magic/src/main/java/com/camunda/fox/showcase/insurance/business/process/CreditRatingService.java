package com.camunda.fox.showcase.insurance.business.process;

import java.util.Random;

import javax.inject.Inject;
import javax.inject.Named;

import com.camunda.fox.showcase.insurance.persistence.entity.AbstractInsuranceApplication;

@Named
public class CreditRatingService  {

  @Inject
  private CreditRatingServiceStatus serviceStatus;
  
  @Inject
  @Current 
  private AbstractInsuranceApplication application;
  
  public void validateApplication() throws Exception {
    
    if( ! serviceStatus.isAvailable()) {
      throw new RuntimeException("This service is not available!");
    }
    
    int randomInt = new Random().nextInt();
    
    if(randomInt % 3 == 0) {
      application.setApproved(false);
      application.setRejectionReason("Bonit�t nicht ausreichend");      
    } else {
      application.setApproved(true);
    }
    
    
  }
  
}
