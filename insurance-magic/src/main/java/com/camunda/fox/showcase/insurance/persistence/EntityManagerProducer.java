package com.camunda.fox.showcase.insurance.persistence;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class EntityManagerProducer {
  
  @PersistenceContext
  private EntityManager entityManager;
  
  @Produces
  public EntityManager getEntityManager() {
    return entityManager;
  }

}
