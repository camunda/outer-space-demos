package com.camunda.fox.showcase.insurance.view.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.DiagramLayout;
import org.activiti.engine.repository.ProcessDefinition;

@Path("/processDiagramLayout")
public class ProcessDiagramLayoutResource {
  
  @Inject
  private RepositoryService repositoryService;
  
  @GET
  @Path("/{key}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getSingleInstance(@PathParam("key") String key) {
    
    ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
      .processDefinitionKey(key)
      .latestVersion()
      .singleResult();
    
    DiagramLayout processDiagramLayout = repositoryService.getProcessDiagramLayout(processDefinition.getId());
    
    return Response
            .ok()
            .entity(processDiagramLayout)
            .type(MediaType.APPLICATION_JSON)
            .build();
    
  }

}
