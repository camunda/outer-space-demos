package com.camunda.fox.showcase.insurance.persistence.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AbstractInsuranceApplication {

  @Id
  @GeneratedValue
  private Long id;

  @Version
  private Long version;
  
  private boolean approved;
  
  private String rejectionReason;
  
  private boolean accepted;

  private String name;
  private String email;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
  
  public boolean isApproved() {
    return approved;
  }
  
  public void setApproved(boolean approved) {
    this.approved = approved;
  }
  
  public String getRejectionReason() {
    return rejectionReason;
  }
  
  public void setRejectionReason(String rejectionReason) {
    this.rejectionReason = rejectionReason;
  }
  
  public void setAccepted(boolean accepted) {
    this.accepted = accepted;
  }
  
  public boolean isAccepted() {
    return accepted;
  }


}
