package com.camunda.fox.showcase.insurance.view.websocket;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;

@Path("/monitor")
@Stateless
@LocalBean
public class MonitoringBean {

  private boolean m_visible = false;

  @POST
  @Path("/toggle")
  public Response toggle(String xml) {
    m_visible = !m_visible;
    notifySubscribers(m_visible ? "ON" : "OFF");
    return Response.ok().build();
  }

  private void notifySubscribers(String state) {
    Broadcaster broadcaster = BroadcasterFactory.getDefault().lookup("control", true);
    if (broadcaster != null)
      broadcaster.broadcast(state);
  }
}
