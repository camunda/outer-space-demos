package com.camunda.fox.showcase.insurance.business.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InsuranceApplicationResult {

  private String requestId;

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

}
