package com.camunda.fox.showcase.insurance.view.jsf;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import com.camunda.fox.showcase.insurance.business.process.CurrentApplicationProvider;
import com.camunda.fox.showcase.insurance.persistence.entity.AbstractInsuranceApplication;

@Named
@Stateless
public class ManualApprovalController {

  @Inject
  private CurrentApplicationProvider currentApplicationProvider;

  private boolean approved;

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    AbstractInsuranceApplication application = currentApplicationProvider.insuranceApplication();
    application.setApproved(approved);
    this.approved = approved;
  }

}
