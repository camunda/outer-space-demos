package com.camunda.fox.showcase.insurance.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * JPA entity represening an insurance product 
 * (vehicle insurance, life insurance ...) 
 *
 */
@Entity
public class Product {

  @Id
  @GeneratedValue
  private Long id;

  @Version
  private Long version;

  private String name;
  
  private String nid;

  private String description;
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
  
  public String getNid() {
    return nid;
  }
  
  public void setNid(String nid) {
    this.nid = nid;
  }


}
