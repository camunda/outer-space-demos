package com.camunda.fox.showcase.insurance.business.process;

import javax.inject.Inject;
import javax.inject.Named;

import com.camunda.fox.showcase.insurance.persistence.entity.AbstractInsuranceApplication;
import com.camunda.fox.showcase.insurance.persistence.entity.TpdInsuranceAppliation;

@Named
public class CheckTpdApplication  {

  @Inject
  @Current 
  private AbstractInsuranceApplication application;
  
  public void validateApplication() {
    
    TpdInsuranceAppliation application = (TpdInsuranceAppliation) this.application;
        
    if(1950 > application.getDateOfBirth()) {
      application.setApproved(false);
      application.setRejectionReason("Geburtsdatum vor 1950");      
      
    } else if(1980 > application.getDateOfBirth() && "Handwerker".equalsIgnoreCase(application.getJob())) {
      application.setApproved(false);
      application.setRejectionReason("Handwerker und Geburtsdatum vor 1980");      
    } else {
      application.setApproved(true);
    }
    
  }

}
