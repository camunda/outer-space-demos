package com.camunda.fox.showcase.insurance.persistence.entity;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class TpdInsuranceAppliation extends AbstractInsuranceApplication {

  private String job;
  private int dateOfBirth;
  private double monthlyPayment;
  private double result;

  public String getJob() {
    return job;
  }

  public void setJob(String job) {
    this.job = job;
  }

  public int getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(int dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public double getMonthlyPayment() {
    return monthlyPayment;
  }

  public void setMonthlyPayment(double monthlyPayment) {
    this.monthlyPayment = monthlyPayment;
  }

  public double getResult() {
    return result;
  }

  public void setResult(double result) {
    this.result = result;
  }

}
