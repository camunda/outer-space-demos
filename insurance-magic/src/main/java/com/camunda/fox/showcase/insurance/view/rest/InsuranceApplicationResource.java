package com.camunda.fox.showcase.insurance.view.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.camunda.fox.showcase.insurance.business.dto.InsuranceApplicationResult;
import com.camunda.fox.showcase.insurance.business.service.InsuranceApplicationService;
import com.camunda.fox.showcase.insurance.persistence.entity.CarInsuranceAppliation;
import com.camunda.fox.showcase.insurance.persistence.entity.TpdInsuranceAppliation;

@Path("application/")
public class InsuranceApplicationResource {
  
  private Logger log = Logger.getLogger(InsuranceApplicationResource.class.getName());
  
  @Inject
  private InsuranceApplicationService insuranceApplicationBusinessLogic;
  
  @POST
  @Path("/car")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response newCarInsuranceApplication(CarInsuranceAppliation application) {
    
    try {
      
      InsuranceApplicationResult insuranceApplicationResponse = insuranceApplicationBusinessLogic.newCarInsuranceApplication(application);
      
      return Response
        .ok()
        .entity(insuranceApplicationResponse)
        .type(MediaType.APPLICATION_JSON)
        .build();
      
    }catch (Exception e) {
      log.log(Level.WARNING, "Error while processing new CarInsuranceAppliationRequest.", e);
      return Response
       .status(Status.INTERNAL_SERVER_ERROR)       
       .build();
    }
           
  }
  
  @POST
  @Path("/tpd")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response newTpdInsuranceApplication(TpdInsuranceAppliation application) {
    
    try {
      
      InsuranceApplicationResult insuranceApplicationResponse = insuranceApplicationBusinessLogic.newTpdInsuranceApplication(application);
      
      return Response
        .ok()
        .entity(insuranceApplicationResponse)
        .type(MediaType.APPLICATION_JSON)
        .build();
      
    }catch (Exception e) {
      log.log(Level.WARNING, "Error while processing new CarInsuranceAppliationRequest.", e);
      return Response
       .status(Status.INTERNAL_SERVER_ERROR)       
       .build();
    }
           
  }
  

}
