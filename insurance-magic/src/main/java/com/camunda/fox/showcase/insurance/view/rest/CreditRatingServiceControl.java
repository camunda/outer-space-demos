package com.camunda.fox.showcase.insurance.view.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.camunda.fox.showcase.insurance.business.process.CreditRatingServiceStatus;

@Path("/control/creditrating")
public class CreditRatingServiceControl {
  
  @Inject
  private CreditRatingServiceStatus creditRatingServiceStatus;
  
  @GET
  public void toggle() {
    creditRatingServiceStatus.setAvailable( ! creditRatingServiceStatus.isAvailable());
  }

}
