package com.camunda.fox.showcase.insurance.view.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("/rest")
public class JaxRsApplication extends Application {
   /* class body intentionally left blank */
}
