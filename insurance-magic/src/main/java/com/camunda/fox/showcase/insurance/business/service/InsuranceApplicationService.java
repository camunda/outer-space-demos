package com.camunda.fox.showcase.insurance.business.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.activiti.cdi.BusinessProcess;
import org.activiti.cdi.annotation.BusinessProcessScoped;

import com.camunda.fox.showcase.insurance.business.dto.InsuranceApplicationResult;
import com.camunda.fox.showcase.insurance.persistence.entity.CarInsuranceAppliation;
import com.camunda.fox.showcase.insurance.persistence.entity.TpdInsuranceAppliation;

@Stateless
public class InsuranceApplicationService {

  @Inject
  private BusinessProcess businessProcess;

  @Inject
  private EntityManager entityManager;
  
  public InsuranceApplicationResult newCarInsuranceApplication(CarInsuranceAppliation application) {

    entityManager.persist(application);
    entityManager.flush();

    Long applicationId = application.getId();
    businessProcess.setVariable("applicationId", applicationId);
    businessProcess.setVariable("applicationType", "car");
    businessProcess.startProcessByMessage("newVehicleInsuranceApplication");

    InsuranceApplicationResult insuranceApplicationResult = new InsuranceApplicationResult();
    insuranceApplicationResult.setRequestId(String.valueOf(applicationId));

    return insuranceApplicationResult;

  }

  public InsuranceApplicationResult newTpdInsuranceApplication(TpdInsuranceAppliation application) {
    
    entityManager.persist(application);
    entityManager.flush();

    Long applicationId = application.getId();
    businessProcess.setVariable("applicationId", applicationId);
    businessProcess.setVariable("applicationType", "tpd");
    businessProcess.startProcessByMessage("newTpdInsuranceApplication");

    InsuranceApplicationResult insuranceApplicationResult = new InsuranceApplicationResult();
    insuranceApplicationResult.setRequestId(String.valueOf(applicationId));
    
    return insuranceApplicationResult;
  }

}
