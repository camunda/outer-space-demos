package com.camunda.activiti.osgi;

import java.beans.FeatureDescriptor;
import java.util.Iterator;

import org.activiti.engine.impl.javax.el.ELContext;
import org.activiti.osgi.blueprint.BlueprintELResolver;
import org.osgi.service.blueprint.container.BlueprintContainer;

public class ContextAwareBlueprintElResolver extends BlueprintELResolver {

  final BlueprintContainer blueprintContainer;

  public ContextAwareBlueprintElResolver(BlueprintContainer container) {
    this.blueprintContainer = container;
  }

  @Override
  public Class< ? > getCommonPropertyType(ELContext context, Object base) {
    return Object.class;
  }

  @Override
  public Class< ? > getType(ELContext context, Object base, Object property) {
    return Object.class;
  }

  @Override
  public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
    return null;
  }

  @Override
  public Object getValue(ELContext context, Object base, Object property) {
    if(base != null){
      return null;
    }
    Object result = blueprintContainer.getComponentInstance((String)property);
    context.setPropertyResolved(true);
    return result;
  }

  @Override
  public boolean isReadOnly(ELContext context, Object base, Object property) {
    return false;
  }

  @Override
  public void setValue(ELContext context, Object base, Object property, Object value) {
  }

}
