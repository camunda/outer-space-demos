package com.camunda.karaf;

import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.apache.karaf.shell.console.OsgiCommandSupport;

@Command(scope = "activiti", name = "list-process-instances", description = "Show a list of process instances")
public class ListProcessInstancesCmd extends OsgiCommandSupport {

	private RuntimeService runtimeService; 
	private RepositoryService repositoryService;
	
	@Argument(index = 0, name = "processdefinitionid", description = "ID of the process definition", required = false, multiValued = false)
	String processdefinitionid;

	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}
	
	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}
	
	@Override
	protected Object doExecute() throws Exception {
		
		List<ProcessInstance> processInstances = null;
		if(processdefinitionid != null) {
			processInstances = runtimeService.createProcessInstanceQuery().processDefinitionId(processdefinitionid).list();
		} else {
			processInstances = runtimeService.createProcessInstanceQuery().list();
		}
		
		for(ProcessInstance pi : processInstances) {
			ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionId(pi.getProcessDefinitionId()).singleResult();
			
			System.out.println("--------------------------------");
			System.out.println("-- Process definition: '" + pd.getName() + " (" + pd.getId() + ")'");
			System.out.println("-- Process instance id: '" + pi.getProcessInstanceId() + "'");
			if(pi.getBusinessKey() != null) {
				System.out.println("-- Business key: '" + pi.getBusinessKey() + "'");
			}
			System.out.println("-- Current activity: " + ((ExecutionEntity) pi).getActivityId());
			System.out.println("--------------------------------");
		}
		
		return null;
	}

}
