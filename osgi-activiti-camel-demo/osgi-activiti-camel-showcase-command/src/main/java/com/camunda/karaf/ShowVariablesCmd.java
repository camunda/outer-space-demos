package com.camunda.karaf;

import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.apache.karaf.shell.console.OsgiCommandSupport;

@Command(scope = "activiti", name = "show-variables", description = "Show the variables of a process instance")
public class ShowVariablesCmd extends OsgiCommandSupport {

	private RuntimeService runtimeService; 
	
	@Argument(index = 0, name = "processinstanceid", description = "ID of the process instance to show the variables for", required = true, multiValued = false)
	String processinstanceid;

	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}
	
	@Override
	protected Object doExecute() throws Exception {
		Map<String, Object> variables = runtimeService.getVariables(processinstanceid);
		
		for(String key : variables.keySet()) {
			System.out.println("'" + key + "': '" + variables.get(key) + "'");
		}
		
		return null;
	}

}
