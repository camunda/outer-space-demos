package com.camunda.karaf;

import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.task.Task;
import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.apache.karaf.shell.console.OsgiCommandSupport;

@Command(scope = "activiti", name = "list-tasks", description = "List tasks of a specific user")
public class ListTasksCmd extends OsgiCommandSupport {

	private TaskService taskService;
	private IdentityService identityService;
	
	@Argument(index = 0, name = "userid", description = "User id", required = true, multiValued = false)
	String userid;

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
	
	public void setIdentityService(IdentityService identityService) {
		this.identityService = identityService;
	}
	
	@Override
	protected Object doExecute() throws Exception {

		List<Task> personalTasks = taskService.createTaskQuery().taskAssignee(userid).list();
	
		if(personalTasks != null) {
			System.out.println("--------------------------------");
			System.out.println("-- " + userid + "'s Tasks ("+ personalTasks.size() +"):");
			System.out.println("--------------------------------");
			printTasks(personalTasks);
		}

		List<Group> groups = identityService.createGroupQuery().groupMember(userid).list();
		
		for(Group group : groups) {
			List<Task> groupTasks = taskService.createTaskQuery().taskCandidateGroup(group.getId()).list();
			System.out.println("--------------------------------");
			System.out.println("-- " + group.getName() + " (" + groupTasks.size() + "):");
			System.out.println("--------------------------------");
			printTasks(groupTasks);
		}

		return null;
	}
	
	private void printTasks(List<Task> tasks) {
		for(Task task : tasks) {
			System.out.println(task.getName() + " (id: '" + task.getId() + "')");
			System.out.println((task.getDescription() != null && task.getDescription().length() > 0) ? task.getDescription() : "- no description -");
			System.out.println("");
		}
	}
}
