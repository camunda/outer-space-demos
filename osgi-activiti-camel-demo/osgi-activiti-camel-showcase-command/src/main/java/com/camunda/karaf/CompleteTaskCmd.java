package com.camunda.karaf;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.TaskService;
import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.apache.karaf.shell.console.OsgiCommandSupport;

@Command(scope = "activiti", name = "complete-task", description = "Complete a task")
public class CompleteTaskCmd extends OsgiCommandSupport {

	private TaskService taskService;
	
	@Argument(index = 0, name = "taskid", description = "Task id", required = true, multiValued = false)
	String taskid;

	@Argument(index = 1, name = "variables", description = "a comma-separated list of key value pairs ('key:value') to pass in task variables", required = false, multiValued = false)
	String variables;
	
	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
	
	@Override
	protected Object doExecute() throws Exception {

		if(variables != null) {
			Map<String, Object> variableMap = new HashMap<String, Object>();
			
			String[] varArray = variables.split(",");
			for(String pair : varArray) {
				String[] variable = pair.split(":");
				variableMap.put(variable[0], variable[1]);
			}
			taskService.complete(taskid, variableMap);
		} else {
			taskService.complete(taskid);
		}
		
		return null;
	}

}
