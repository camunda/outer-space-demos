package com.camunda.demo.bean;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.camunda.demo.data.Order;

public class IncomingOrderService {

	public Map<String, Object> orderToMap(String orderXml) throws JAXBException {
		
		final JAXBContext jaxbContext = JAXBContext.newInstance(Order.class);
		final Order order = (Order) jaxbContext.createUnmarshaller()
				.unmarshal(new StringReader(orderXml));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ordernumber", order.getOrdernumber());
		map.put("firstname", order.getPerson().getFirstname());
		map.put("lastname", order.getPerson().getLastname());
		map.put("accounttype", order.getAccounttype());
		
		return map;
	}
	
}
