package com.camunda.demo.route;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

import com.camunda.demo.bean.IncomingOrderService;

public class OpenAccountRouteBuilder extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		// read incoming orders and start the account opening process
		from("file://osgi-activiti-camel-showcase/orders").
			log(LoggingLevel.INFO, "Recieved order with content: '${body}'").
			// use the order number as business key for the activiti process 
			setProperty("PROCESS_KEY_PROPERTY").
			xpath("//@ordernumber").
			// transform xml order into an order object and place it in a 
			// map for activiti to be able to treat it as a process variable  
			bean(IncomingOrderService.class).
			// route the variable map to activiti and start an instance of the 
			// "open-account" process
			to("activiti:open-account");

		// read incoming postident scans and correlate them to the appropriate process
		from("file://osgi-activiti-camel-showcase/postident").
			// extract the order number from the file name and use it to 
			// correlate the document to the process it belongs to
			process(new Processor() {
				@Override
				public void process(Exchange exchange) throws Exception {
					String businessKey = exchange.getIn().getHeader("CamelFileName").toString().split("-")[1].substring(0, 4);
					exchange.setProperty("PROCESS_KEY_PROPERTY", businessKey);
				}
			}).
			to("activiti:open-account:wait_for_postident");

	}

}
