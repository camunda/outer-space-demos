package com.camunda.demo;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.test.Deployment;
import org.activiti.spring.impl.test.SpringActivitiTestCase;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.springframework.test.context.ContextConfiguration;

import com.camunda.demo.data.Address;
import com.camunda.demo.data.Order;
import com.camunda.demo.data.Person;

@ContextConfiguration("classpath:camel-test-context.xml")
public class OpenAccountProcessTest extends SpringActivitiTestCase {

	private Order order;
	private JAXBContext jaxbContext;
	private StringWriter stringWriter;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		Person person = new Person("John", "Doe", "Mr.", new Date(0), "Some Place", "male", "123456", "john.doe@somecompany.com");
		Address address = new Address("The Street", 1, "1234", "The City", "The State", "The Country");
		order = new Order("0001", person, address, "debit");
		
		jaxbContext = JAXBContext.newInstance(Order.class);
		stringWriter = new StringWriter();
	}
	
	public void testMarshal() throws JAXBException {
		jaxbContext.createMarshaller().marshal(order, stringWriter);

		assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
	    		"<order ordernumber=\"0001\">" +
	    		"<person>" +
	    		"<firstname>John</firstname>" +
	    		"<lastname>Doe</lastname>" +
	    		"<title>Mr.</title>" +
	    		"<dateofbirth>1970-01-01T01:00:00+01:00</dateofbirth>" +
	    		"<placeofbirth>Some Place</placeofbirth>" +
	    		"<gender>male</gender>" +
	    		"<phonenumber>123456</phonenumber>" +
	    		"<email>john.doe@somecompany.com</email>" +
	    		"</person>" +
	    		"<address>" +
	    		"<street>The Street</street>" +
	    		"<number>1</number>" +
	    		"<zipcode>1234</zipcode>" +
	    		"<city>The City</city>" +
	    		"<state>The State</state>" +
	    		"<country>The Country</country>" +
	    		"</address>" +
	    		"<accounttype>debit</accounttype>" +
	    		"</order>", stringWriter.toString());
		
		final Order orderRead = (Order) jaxbContext.createUnmarshaller().unmarshal(new StringReader(stringWriter.toString()));
		assertEquals(order, orderRead);
	}
	
	@Deployment(resources="OSGI-INF/activiti/open-account.bpmn20.xml")
	public void testProcessStartAndCorrelation() throws InterruptedException, JAXBException {
		CamelContext ctx = applicationContext.getBean(CamelContext.class);

		jaxbContext.createMarshaller().marshal(order, stringWriter);
		String orderXml = stringWriter.toString(); 
		
		ProducerTemplate template = ctx.createProducerTemplate();
	    template.sendBodyAndHeader(
	    		"file://osgi-activiti-camel-showcase/orders",
	    		orderXml, Exchange.FILE_NAME,
	    		"order-0001.xml");
	    
	    Thread.sleep(1000);

	    ProcessInstance pi = runtimeService.createProcessInstanceQuery().singleResult();
	    assertNotNull(pi);
	    String orderNumber = (String)runtimeService.getVariable(pi.getId(), "ordernumber"); 
	    assertNotNull(orderNumber);
	    
	    String firstname =  (String)runtimeService.getVariable(pi.getId(), "firstname");
	    assertNotNull(firstname);
	    
	    String lastname =  (String)runtimeService.getVariable(pi.getId(), "lastname");
	    assertNotNull(lastname);

	    String accounttype =  (String)runtimeService.getVariable(pi.getId(), "accounttype");
	    assertNotNull(accounttype);
	    
	    template.sendBodyAndHeader("file://osgi-activiti-camel-showcase/postident",
	    		"This would be a PDF in real life...", Exchange.FILE_NAME,
	    		"hello-0001.pdf");

	    Thread.sleep(1000);
	    
	    pi = runtimeService.createProcessInstanceQuery().singleResult();
	    
	    assertNotNull(pi);
	    assertEquals("check_documents", ((ExecutionEntity)pi).getActivityId());

	    runtimeService.signal(pi.getId());
	    
	    assertProcessEnded(pi.getId());
	}
}
