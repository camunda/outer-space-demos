Installing and starting the Showcase
------------------------------------

- Download and install apache karaf 2.2.5
- start karaf (<install-dir>/bin/karaf)
- add camel features:
> features:addurl mvn:org.apache.camel.karaf/apache-camel/2.9.1/xml/features
> features:install camel
> features:install camel-blueprint

- Build and install showcase to local maven repo (mvn clean install), then (in karaf) type:
> features:addurl mvn:com.camunda.demo.activiti.karaf/osgi-activiti-camel-showcase-features/0.0.1-SNAPSHOT/xml/features
> features:install osgi-activiti-camel-showcase

(you can use features:refreshurl if you re-build)

- then install the demo process
> osgi:install -s mvn:com.camunda.demo.activiti.karaf/osgi-activiti-camel-showcase-process/0.0.1-SNAPSHOT

----------------------------
Karaf/ServiceMix Cheat Sheet
----------------------------

Installing and managing features:
---------------------------------
features:addurl ...

Install camel
-------------
> features:addurl mvn:org.apache.camel.karaf/apache-camel/2.8.2/xml/features
> features:install camel

Installing custom modules
-------------------------
> osgi:install -s mvn:<groupId>/<artifactId>/<version>

Useful Links
------------
camel file: http://camel.apache.org/file.html
camel examples: http://camel.apache.org/examples.html
Karaf Tutorial: http://www.liquid-reality.de/display/liquid/2012/01/03/Karaf+Tutorial+Part+5+-+Running+Apache+Camel+integrations+in+OSGi

